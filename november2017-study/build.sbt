name         := "november2017-study"
organization := "pairwiseltd"
version := "0.1.0-SNAPSHOT"
scalaVersion := "2.11.8"

libraryDependencies += "junit" % "junit" % "4.10"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.4"
libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.13.4"
