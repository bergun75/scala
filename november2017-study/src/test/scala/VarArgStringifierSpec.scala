
import org.scalatest.Matchers
import org.scalatest.WordSpec

class VarArgStringifierSpec extends WordSpec with Matchers {

  "VarArgStringifier object to string method" should {
    "convert varags to String" when {
      "called" in {
        VarArgStringifier.apply("2", "3", "5", "8").a shouldBe "2,3,5,8"
      }
    }
  }
}