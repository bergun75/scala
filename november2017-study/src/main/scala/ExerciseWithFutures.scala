import scala.concurrent.Future
import scala.concurrent.Await
import scala.concurrent.duration.Duration


object ExerciseWithFutures extends App {  
  import scala.concurrent.ExecutionContext.Implicits.global
  val futuresList = Future.traverse((1 to 100).toList)(x => Future(x * 2 - 1))
  val results = Await.result(futuresList, Duration.Inf)
  val oddSum = results.sum
  println(oddSum)
  
  val futuresSequence = Future.sequence((1 to 100).toList map (x => Future(x * 2 - 1)))
  val results2= Await.result(futuresSequence, Duration.Inf)
  val oddSum2 = results.sum
  println(oddSum2)
}