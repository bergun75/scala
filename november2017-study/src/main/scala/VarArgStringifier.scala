import scala.collection.mutable.WrappedArray

class VarArgStringifier(args: Seq[String]){
  def a:String = {   
     args.mkString(",")   
  }
}

object VarArgStringifier {
  def apply(args: String*) = new VarArgStringifier(args)
}