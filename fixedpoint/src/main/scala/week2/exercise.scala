package week2
import math.abs
import scala.annotation.tailrec

object exercise {
  val TOLERANCE = 0.0001
  def isCloseEnough(x: Double, y: Double) =
    abs((x - y) / x) / x < TOLERANCE
  def fixedPoint(f: Double => Double)(firstGuess: Double) = {
    def iterate(guess: Double): Double = {
      val next = f(guess)
      if (isCloseEnough(guess, next)) next
      else iterate(next)
    }
    iterate(firstGuess)
  }
  def averageDamp(f: Double => Double)(x: Double) = (x + f(x))/ 2
  def sqrt(x: Double) = fixedPoint(averageDamp(y => x/y))(1)
  def main(args: Array[String]) {
    println(fixedPoint(x => 1 + x / 2)(1))
    println(sqrt(2))
  }

}