
import common._

package object scalashop {

  /** The value of every pixel is represented as a 32 bit integer. */
  type RGBA = Int

  /** The value of every pixel is represented as a 32 bit integer. */
  type RGBA_TUPLE = (Int, Int, Int, Int)

  /** Returns the red component. */
  def red(c: RGBA): Int = (0xff000000 & c) >>> 24

  /** Returns the green component. */
  def green(c: RGBA): Int = (0x00ff0000 & c) >>> 16

  /** Returns the blue component. */
  def blue(c: RGBA): Int = (0x0000ff00 & c) >>> 8

  /** Returns the alpha component. */
  def alpha(c: RGBA): Int = (0x000000ff & c) >>> 0

  /** Used to create an RGBA value from separate components. */
  def rgba(r: Int, g: Int, b: Int, a: Int): RGBA = {
    (r << 24) | (g << 16) | (b << 8) | (a << 0)
  }

  def decompose(pixel: RGBA): RGBA_TUPLE =
    (red(pixel), green(pixel), blue(pixel), alpha(pixel))

  def sumPixel(pixel1: RGBA_TUPLE, pixel2: RGBA_TUPLE): RGBA_TUPLE =
    ((pixel1._1 + pixel2._1),
      (pixel1._2 + pixel2._2),
      (pixel1._3 + pixel2._3),
      (pixel1._4 + pixel2._4))

  /** Restricts the integer into the specified range. */
  def clamp(v: Int, min: Int, max: Int): Int = {
    if (v < min) min
    else if (v > max) max
    else v
  }

  /** Image is a twoclamp-dimensional matrix of pixel values. */
  class Img(val width: Int, val height: Int, private val data: Array[RGBA]) {
    def this(w: Int, h: Int) = this(w, h, new Array(w * h))
    def apply(x: Int, y: Int): RGBA = data(y * width + x)
    def update(x: Int, y: Int, c: RGBA): Unit = data(y * width + x) = c
  }

  /** Computes the blurred RGBA value of a single pixel of the input image. */
  def boxBlurKernel(src: Img, x: Int, y: Int, radius: Int): RGBA = {
    if (radius == 0) src.apply(x, y)
    else {
      var dPixel = (0, 0, 0, 0)
      val xEnd: Int = clamp(x + radius, 0, src.width - 1)
      val yEnd: Int = clamp(y + radius, 0, src.height - 1)
      val xBeg: Int = clamp(x - radius, 0, src.width - 1)
      val yBeg: Int = clamp(y - radius, 0, src.height - 1)
      var xPos: Int = xBeg
      var yPos: Int = yBeg
      val totalPixels: Int = (xEnd - xBeg + 1) * (yEnd - yBeg + 1)
      while (xPos <= xEnd) {
        yPos = yBeg
        while (yPos <= yEnd) {
          val curPixel = decompose(src.apply(xPos, yPos))
          dPixel = sumPixel(dPixel, curPixel)
          yPos = yPos + 1;
        }
        xPos = xPos + 1;
      }
      rgba((dPixel._1 / totalPixels),
        (dPixel._2 / totalPixels),
        (dPixel._3 / totalPixels),
        (dPixel._4 / totalPixels))
    }
  }

}
