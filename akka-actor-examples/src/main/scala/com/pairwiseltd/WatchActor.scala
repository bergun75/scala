package com.pairwiseltd

import akka.actor.Actor
import akka.actor.ActorSystem
import akka.actor.Props
import akka.actor.Terminated
import akka.event.Logging
import akka.actor.CoordinatedShutdown
import scala.concurrent.Await
import scala.concurrent.Future
import scala.concurrent.duration._
import akka.Done

object WatchActor extends App {
  val system = ActorSystem("NewActorSystem")
  val actor = system.actorOf(Props[WatchActor], "parent")
  actor ! "kill"
  Thread.sleep(2000)
  val done: Future[Done] = CoordinatedShutdown(system).run()
  import system.dispatcher
  val f2 = done map (x => println("waiting to shutdown"))
  Await.result(f2, 5000 millis);
}

class WatchActor extends Actor {
  val logger = Logging(context.system, this)
  val child = context.actorOf(Props.empty, "child")
  context.watch(child)
  var lastSender = context.system.deadLetters

  def receive = {
    case "kill" =>
      context.stop(child)
      logger.info(s"Stopping $self")
      lastSender = sender()
    case Terminated(`child`) =>
      logger.info(s"Stopped $child")
      lastSender ! "finished"
  }
}