package com.pairwiseltd

import scala.concurrent.Future

import akka.Done
import akka.actor.Actor
import akka.actor.ActorSystem
import akka.actor.Props
import akka.event.Logging
import akka.actor.CoordinatedShutdown
import scala.concurrent.Await
import scala.concurrent.duration._

object DemoActor {
  /**
   * Create Props for an actor of this type.
   *
   * @param magicNumber The magic number to be passed to this actor’s constructor.
   * @return a Props for creating this actor, which can then be further configured
   *         (e.g. calling `.withDispatcher()` on it)
   */
  def props(magicnumber: Int): Props = Props(classOf[DemoActor], magicnumber)
  def main(args: Array[String]): Unit = {
    val system = ActorSystem("MyActorSystem")
    val someOtherActor = system.actorOf(Props[SomeOtherActor], "parent")
    someOtherActor ! Ignite("start lan!")
    val done: Future[Done] = CoordinatedShutdown(system).run()
    import system.dispatcher
    val f2 = done map (x => println("waiting to shutdown"))
    Await.result(f2, 5000 millis);
  }
}

class DemoActor(magicnumber: Int) extends Actor {
  val log = Logging(context.system, this)
  def receive = {
    case x: Int =>
      log.info(s"Received $x")
      sender() ! ProcessNumber(x + magicnumber)
  }
}

/*
 * It is a good idea to provide factory methods on the companion object of each Actor which help keeping the 
 * creation of suitable Props as close to the actor definition as possible. This also avoids the pitfalls 
 * associated with using the Props.apply(...) method which takes a by-name argument, since within a companion 
 * object the given code block will not retain a reference to its enclosing scope:
 */

class SomeOtherActor extends Actor {
  val demoActor = context.actorOf(DemoActor.props(42), "child")
  val log = Logging(context.system, this)
  def receive = {
    case Ignite(x) =>
      val f = scala.util.Random.nextInt()
      log.info(s"SomeOtherActor triggered with message =$x and with random number $f")
      demoActor ! f
    case ProcessNumber(y) =>
      log.info(s"sum result is =$y")
  }
}

/* Another good practice is to declare what messages an Actor can receive in the companion object of the Actor, 
 * which makes easier to know what it can receive:
 * 
 */
case class Ignite(message: String)
case class ProcessNumber(number: Int)