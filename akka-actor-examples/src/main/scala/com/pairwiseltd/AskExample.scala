package com.pairwiseltd

/*
* This example demonstrates ask together with the pipeTo pattern on futures, because this is likely
* to be a common combination. Please note that all of the above is completely non-blocking and asynchronous: 
* ask produces a Future, three of which are composed into a new future using the for-comprehension and 
* then pipeTo installs an onComplete-handler on the future to affect the submission of the 
* aggregated Result to another actor.

* Using ask will send a message to the receiving Actor as with tell, and the receiving actor must 
* reply with sender() ! reply in order to complete the returned Future with a value. 
* The ask operation involves creating an internal actor for handling this reply, which needs to have a timeout after 
* which it is destroyed in order not to leak resources; see more below.
* WARNING
*  To complete the future with an exception you need send a Failure message to the sender. 
*  This is not done automatically when an actor throws an exception while processing a message.
 */

import scala.concurrent.duration.DurationInt

import akka.util.Timeout
import akka.actor.Props
import akka.actor.Actor
import akka.event.Logging
import akka.actor.ActorSystem
import scala.concurrent.Future
import akka.pattern.{ ask, pipe }
import akka.actor.PoisonPill
import akka.actor.CoordinatedShutdown
import scala.concurrent.Await
import akka.Done

final case class Result(x: Int, s: String, d: Double)
sealed trait Request
case object RequestString extends Request
case object RequestInt extends Request
case object RequestDouble extends Request
case class RequestResult(x: Result) extends Request

object AskExample extends App {
  implicit val timeout = Timeout(5 seconds)
  val system = ActorSystem("NewActorSystem")
  val actor = system.actorOf(Props[WiseActor], "wise")

  import system.dispatcher
  val f: Future[Result] =
    for {
      x <- (actor ? RequestInt).mapTo[Int]
      s <- (actor ? RequestString).mapTo[String]
      d <- (actor ? RequestDouble).mapTo[Double]
    } yield Result(x, s, d)

  pipe(f) to actor

  val done: Future[Done] = CoordinatedShutdown(system).run()
  import system.dispatcher
  val f2 = done map (x => println("waiting to shutdown"))
  Await.result(f2, 5000 millis);  
}

class WiseActor extends Actor {
  val log = Logging(context.system, this)
  def receive = {
    case RequestString =>
      log.info(s"String request received")
      sender ! scala.util.Random.nextString(10)
    case RequestInt =>
      log.info(s"Int request received")
      sender ! scala.util.Random.nextInt
    case RequestDouble =>
      log.info(s"Double request received")
      sender ! scala.util.Random.nextDouble
    case Result(x, s, d) =>
      log.info(s"Result request received = $x $s $d")      
  }
}

