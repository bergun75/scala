package com.pairwiseltd.stopping

import scala.concurrent.Future
import scala.util.control.TailCalls.Done

import akka.Done
import akka.actor.Actor
import akka.actor.ActorSystem
import akka.actor.CoordinatedShutdown
import akka.actor.PoisonPill
import akka.actor.Props
import akka.event.Logging
import scala.concurrent.Await
import scala.concurrent.duration._

object StoppingActorsPoisionPill extends App {
  val system = ActorSystem("NewActorSystem")
  val actor = system.actorOf(Props[MyActor2], "myActor")
  actor ! "Hello"
  actor ! "Hello"
  actor ! "Hello"
  actor ! PoisonPill
  
  val done: Future[Done] = CoordinatedShutdown(system).run()
  import system.dispatcher
  val f2 = done map (x => println("waiting"))
  Await.result(f2, 5000 millis);

}

class MyActor2 extends Actor {
  val logger = Logging(context.system, this)

  def receive = {
    case "Hello" =>
      logger.info("Hello received")
    case x =>
      logger.info(s"message $x received")

  }

  override def postStop() {
    logger.info("post stop called")

  }
}
