package com.pairwiseltd.stopping

import scala.concurrent.Await
import scala.concurrent.Future
import scala.concurrent.duration._

import akka.Done
import akka.actor.Actor
import akka.actor.ActorSystem
import akka.actor.CoordinatedShutdown
import akka.actor.Props
import akka.event.Logging

object StoppingActors extends App {
  val system = ActorSystem("NewActorSystem")
  val actor = system.actorOf(Props[MyActor], "myActor")
  actor ! "Hello"
  actor ! "Hello"
  actor ! "Hello"
  actor ! "Stop"
  val done: Future[Done] = CoordinatedShutdown(system).run()
  import system.dispatcher
  val f2 = done map (x => println("waiting to shutdown"))
  Await.result(f2, 5000 millis);
}

class MyActor extends Actor {
  val logger = Logging(context.system, this)

  def receive = {
    case "Hello" =>
      logger.info("Hello received")
    case "Stop" =>
      logger.info("Stop received")
      context stop self
    case x =>
      logger.info(s"message $x received")

  }

  override def postStop() {
    logger.info("post stop called")
  }
}
