package com.pairwiseltd.timeout

import scala.concurrent.Await
import scala.concurrent.Future
import scala.concurrent.duration._


import akka.Done
import akka.actor.Actor
import akka.actor.ActorSystem
import akka.actor.CoordinatedShutdown
import akka.actor.Props
import akka.actor.ReceiveTimeout
import akka.actor.actorRef2Scala
import akka.event.Logging

object ReceiveTimeoutExample extends App {
  val system = ActorSystem("NewActorSystem")
  val actor = system.actorOf(Props[MainActor], "main")
  actor ! "Start"
  val done: Future[Done] = CoordinatedShutdown(system).run()
  import system.dispatcher
  val f2 = done map (x => println("waiting shutdown"))
  Await.result(f2, 5000 millis);
}

class MyActor extends Actor {
  val logger = Logging(context.system, this)
  context.setReceiveTimeout(30 milliseconds)
  def receive = {
    case "Hello" =>
      logger.info("Hello received")
      context.setReceiveTimeout(100 milliseconds)
    case ReceiveTimeout =>
      // To turn it off      
      context.setReceiveTimeout(Duration.Undefined)
      logger.info("Timeout received")
    // throw new RuntimeException("Receive timed out")
    // if above exception thrown this actor will be completely stopped 
    // ie empty Path and no incarnation exists
  }
}

class MainActor extends Actor {
  val logger = Logging(context.system, this)
  val child = context.actorOf(Props[MyActor], "myActor")
  def receive = {
    case "Start" =>
      child ! "Hello"
      Thread.sleep(10)
      child ! "Hello"
      Thread.sleep(10)
      child ! "Hello"
      Thread.sleep(150)
      child ! "Hello"    
  }
}

/*
*	Messages marked with NotInfluenceReceiveTimeout will not reset the timer. This can be useful when 
* ReceiveTimeout should be fired by external inactivity but not influenced by internal activity, e.g. 
* scheduled tick messages.
*/
