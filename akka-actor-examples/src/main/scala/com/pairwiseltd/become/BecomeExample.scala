package com.pairwiseltd.become

import akka.actor.AbstractActor.Receive
import akka.actor.Actor
import akka.actor.ActorSystem
import akka.actor.CoordinatedShutdown
import scala.concurrent.Await
import scala.concurrent.Future
import scala.concurrent.duration._
import akka.pattern.gracefulStop
import akka.actor.Props
import akka.Done
import akka.event.Logging
import akka.actor.PoisonPill
import java.lang.Shutdown
import java.lang.Shutdown
import java.lang.Shutdown
import akka.actor.Terminated

object BecomeExample extends App {

  val system = ActorSystem("MyActorSystem")
  val mainActor = system.actorOf(Props[MainActor], "mainActor")
  mainActor ! Mood("dudu")
  mainActor ! Mood("angry")
  mainActor ! Mood("happy")
  mainActor ! Mood("happy")
  mainActor ! Mood("angry")
  mainActor ! Mood("angry")
  mainActor ! Mood("dudu") // notice that this ignored! because the original recieve method
  // is hotswapped
  Thread.sleep(2000)
  
  // now observe graceful stop of main actor
  try {    
    val stopped: Future[Boolean] = gracefulStop(mainActor, 5 seconds, MainActor.Shutdown)
    Await.result(stopped, 2 seconds)
    // the actor has been stopped
  } catch {    
    case e: akka.pattern.AskTimeoutException =>
      println("the actor wasn't stopped within 5 seconds")
  }
  
  // now Coordinated shutdown of whole system
  val done: Future[Done] = CoordinatedShutdown(system).run()
  import system.dispatcher
  val f2 = done map (x => println("shut down Actor System"))
  Await.result(f2, 5000 millis);
}

object MainActor {
  case object Shutdown
}

class MainActor extends Actor {
  val log = Logging(context.system, this)
  val child = context.actorOf(Props[HotSwapActor], "hotSwap")
  val worker = context.watch(child)
  import MainActor._
  import context._
  def receive = {
    case Mood(x) =>
      log.info(s"sending mood $x to child")
      child ! x
    case x: String => log.info(x)
    case Shutdown =>
      log.info("actor shutting down")
      child ! PoisonPill
      become(shuttingDown)      
      
  }

  def shuttingDown: Receive = {
    case Terminated(`child`) =>
      log.info("child poisoned")
      context stop self
    case x =>
      log.info(s"service unavailable, shutting down $x")
      sender() ! "service unavailable, shutting down"
  }

}

class HotSwapActor extends Actor {
  val log = Logging(context.system, this)
  import context._
  def angry: Receive = {
    case "angry" => sender ! "I am already angry :-/"
    case "happy" =>
      sender ! "I am becoming happy!"
      become(happy)
  }

  def happy: Receive = {
    case "happy" => sender() ! "I am already happy :-)"
    case "angry" =>
      sender ! "I am becoming angry!"
      become(angry)
  }

  def receive = {
    case "angry" => become(angry)
    case "happy" => become(happy)
    case "dudu"  => sender ! "what the hell!"
  }
}

case class Mood(x: String)