package com.pairwiseltd

import akka.actor.Actor
import akka.event.Logging
import akka.actor.ActorSystem
import akka.actor.Props
import akka.actor.CoordinatedShutdown
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.concurrent.Future
import akka.Done

object MyActorExample {
  def main(args: Array[String]): Unit = {
    val system = ActorSystem("MyActorSystem")
    val myActor = system.actorOf(Props[MyActor], name = "myActor1")    
    val myActor2 = system.actorOf(Props(classOf[MyActor2], "actor2"), name = "myActor2")
    myActor ! "test"
    myActor ! "budala"
    myActor2 ! "test"
    myActor2 ! "budala"    
    val done: Future[Done] = CoordinatedShutdown(system).run()
    import system.dispatcher
    val f2 = done map (x => println("waiting to shutdown"))
    Await.result(f2, 5000 millis);
  }

}

/*
 *  The receive method should define a series of case statements (which has the type PartialFunction[Any, Unit]) 
 *  that defines which messages your Actor can handle, using standard Scala pattern matching, along 
 *  with the implementation of how the messages should be processed.
 */
class MyActor extends Actor {
  val log = Logging(context.system, this)

  def receive = {
    case "test" => log.info("received test")
    case any    => log.info(s"received unknown message $any")
    /*
     * Please note that the Akka Actor receive message loop is exhaustive, which is different compared to Erlang 
     * and the late Scala Actors. This means that you need to provide a pattern match for all messages that 
     * it can accept and if you want to be able to handle unknown messages then you need 
     * to have a default case as in the example above. Otherwise an akka.actor.UnhandledMessage(message, sender, recipient) 
     * will be published to the ActorSystem's EventStream.
     */
  }
}

class MyActor2(param: String) extends Actor {
  val log = Logging(context.system, this)

  def receive = {
    case "test" => log.info(s"$param: received test")
    case any    => log.info(s"$param: received unknown message $any")
    /*
     * Please note that the Akka Actor receive message loop is exhaustive, which is different compared to Erlang 
     * and the late Scala Actors. This means that you need to provide a pattern match for all messages that 
     * it can accept and if you want to be able to handle unknown messages then you need 
     * to have a default case as in the example above. Otherwise an akka.actor.UnhandledMessage(message, sender, recipient) 
     * will be published to the ActorSystem's EventStream.
     */
  }
}

/*
 * Unsupported Edge Cases.
 * 1-) An actor with AnyVal arguments
 * 2-) An actor with default constructor values
 * In both cases IllegalArgumentException will be thrown
 * 
 */


