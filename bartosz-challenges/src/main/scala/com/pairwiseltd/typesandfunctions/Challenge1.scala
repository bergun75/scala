package com.pairwiseltd.typesandfunctions

object Challenge1 extends App {

  val memInt = new MemoizeFunc[Int]
  def f(x: Int): Int = {
    Thread.sleep(400)
    x * 3
  }
  (0 to 3) foreach (x => {
    val memoizedFunc = memInt.g(f)(x)
    println(memoizedFunc(x))
  })
  (0 to 3) foreach (x => {
    val memoizedFunc = memInt.g(f)(x)
    println(memoizedFunc(x))
    println(memoizedFunc(x*4))
  })
}

class MemoizeFunc[A] {
  val cache = scala.collection.mutable.Map.empty[A, A => A]
  def g(f: A => A)(a: A): A => A = {
    if (!cache.isDefinedAt(a)) {
      val x = f(a)
      cache.put(a, a => x)
    }
    a => {
      val res = cache.getOrElse(a, f)
      res(a)
    }
  }
}
