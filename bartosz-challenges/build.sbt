lazy val commonSettings = Seq(
  name         := "bartosz-challenges",
  organization := "pairwiseltd",
  version := "0.1.0-SNAPSHOT",
  scalaVersion := "2.11.8"
)

lazy val root = (project in file("."))
  .settings(
    commonSettings,        
    libraryDependencies += "junit" % "junit" % "4.10" % "test"
)
