package client

import java.util.UUID

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpMethods, HttpRequest, HttpResponse, StatusCodes, Uri}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, Sink, Source}
import client.PaymentSystemDomain.PaymentRequest

import scala.util.{Failure, Success, Try}
import spray.json._

object HostLevel extends PaymentJsonProtocol {
  def main(args: Array[String]): Unit = {
    implicit val system = ActorSystem("HostLevel")
    implicit val materializer = ActorMaterializer()
    import system.dispatcher
    // provides a connection pool. High volume and low latency requests.

    val poolFlow: Flow[(HttpRequest, Int), (Try[HttpResponse], Int), Http.HostConnectionPool]
    = Http().cachedHostConnectionPool[Int]("www.google.com")

//    Source(1 to 10)
//      .map(i => (HttpRequest(), i))
//      .via(poolFlow)
//      .map {
//        case (Success(resp), value) =>
//          // VERY IMPORTANT discard bytes
//          resp.discardEntityBytes()
//          s"Request $value has received response: $resp"
//        case (Failure(ex), value) =>
//          s"Request $value has failed: $ex"
//      }
//      .runWith(Sink.foreach[String](println))

    val creditCards = List(
      CreditCard("4242-4242-4242-4242", "424", "tx-test-account"),
      CreditCard("1234-1234-1234-1234", "123", "tx-someone-account"),
      CreditCard("1234-1234-4321-2222", "321", "tx-blablas-account")
    )

    val paymentRequests = creditCards.map(creditCard => PaymentRequest(creditCard, "rtivm-store-account", 99.0))
    val serverHttpRequests = paymentRequests.map(paymentRequest =>
      (
        HttpRequest(
          HttpMethods.POST,
          uri = Uri("/api/payments"),
          entity = HttpEntity(
            ContentTypes.`application/json`,
            paymentRequest.toJson.prettyPrint
          )
        ),
        UUID.randomUUID().toString
      )
    )

    Source(serverHttpRequests)
      .via(Http().cachedHostConnectionPool[String]("localhost", 8080))
      .runForeach{
        case (Success(response@HttpResponse(StatusCodes.Forbidden, _, _, _)), orderId) =>
          println(s"The orderid $orderId was denied by the system and returned response $response")
        case (Success(response@HttpResponse(StatusCodes.OK, _, _,_)), orderId) =>
          println(s"The orderid $orderId was success and returned response $response")
          // do something with the order ID : dispatch it send notification to the customer.
        case (Failure(ex), orderId) => println(s"The orderid $orderId failed with exception $ex")
      }
  }
  // Should be used for High Volume and low latency requests.
  // Do not use for one-off requests
  // Do not used for long-lived requests.
}
