package client

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpMethods, HttpRequest, Uri}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import client.PaymentSystemDomain.PaymentRequest

import scala.util.{Failure, Success}
import spray.json._

object RequestLevel extends PaymentJsonProtocol {

  def main(args: Array[String]): Unit = {
    implicit val system = ActorSystem("RequestLevel")
    implicit val materializer = ActorMaterializer()
    import system.dispatcher

//    val responseFuture = Http().singleRequest(HttpRequest(uri = "http://www.google.com"))
//
//    responseFuture.onComplete{
//      case Success(resp) =>
//        resp.discardEntityBytes()
//        println(s"The request was successful and returned: $resp")
//      case Failure(ex) =>
//        println(s"The request failed with ex: $ex")
//    }

    val creditCards = List(
      CreditCard("4242-4242-4242-4242", "424", "tx-test-account"),
      CreditCard("1234-1234-1234-1234", "123", "tx-someone-account"),
      CreditCard("1234-1234-4321-2222", "321", "tx-blablas-account")
    )

    val paymentRequests = creditCards.map(creditCard => PaymentRequest(creditCard, "rtivm-store-account", 99.0))
    val serverHttpRequests = paymentRequests.map(paymentRequest =>
      HttpRequest(
        HttpMethods.POST,
        uri = Uri("http://localhost:8080/api/payments"),
        entity = HttpEntity(
          ContentTypes.`application/json`,
          paymentRequest.toJson.prettyPrint
        )
      )
    )

    Source(serverHttpRequests)
      .mapAsyncUnordered(10)(request => Http().singleRequest(request))
      .runForeach(println)
  }

  // low- volume, low latency requests.

}
