package client

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpMethods, HttpRequest, HttpResponse, Uri}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Sink, Source}
import client.PaymentSystemDomain.PaymentRequest

import scala.concurrent.Future
import scala.util.{Failure, Success}
import spray.json._

object ConnectionLevel extends PaymentJsonProtocol {

  def main(args: Array[String]): Unit = {
    implicit val system = ActorSystem("ConnectionLevel")
    implicit val materializer = ActorMaterializer()
    import system.dispatcher
    val connectionFlow = Http().outgoingConnection("www.google.com")

    def oneOffRequest(request: HttpRequest): Future[HttpResponse] =
      Source.single(request).via(connectionFlow).runWith(Sink.head)

    oneOffRequest(HttpRequest()).onComplete {
      case Success(response) => println(s"Got successful response: $response")
      case Failure(ex) => println(s"Sending request failed: $ex")
    }

    /*
    A small payment system
     */

    val creditCards = List(
      CreditCard("4242-4242-4242-4242", "424", "tx-test-account"),
      CreditCard("1234-1234-1234-1234", "123", "tx-someone-account"),
      CreditCard("1234-1234-4321-2222", "321", "tx-blablas-account")
    )

    val paymentRequests = creditCards.map(creditCard => PaymentRequest(creditCard, "rtivm-store-account", 99.0))
    val serverHttpRequests = paymentRequests.map(paymentRequest =>
      HttpRequest(
        HttpMethods.POST,
        uri = Uri("/api/payments"),
        entity = HttpEntity(
          ContentTypes.`application/json`,
          paymentRequest.toJson.prettyPrint
        )
      )
    )

    Source(serverHttpRequests).via(Http().outgoingConnection("localhost", 8080))
      .to(Sink.foreach[HttpResponse](println))
      .run()

  }

  // better to use with long lived requests!
}
