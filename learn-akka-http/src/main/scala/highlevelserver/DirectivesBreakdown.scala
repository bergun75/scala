package highlevelserver

import akka.actor.ActorSystem
import akka.event.LoggingAdapter
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpRequest, StatusCodes}
import akka.stream.ActorMaterializer

object DirectivesBreakdown {
  def main(args: Array[String]): Unit = {
    implicit val system = ActorSystem("DirectivesBreakdown")
    implicit val materializer = ActorMaterializer()
    import akka.http.scaladsl.server.Directives._
    // Type 1 filtering directives
    val simpleHttpMethodRoute =
      post {
        complete(StatusCodes.Forbidden)
      }

    val simplePathRoute =
      path("about") {
        complete(
          HttpEntity(
            ContentTypes.`text/html(UTF-8)`,
            """
              |<html>
              | <body>
              | Hello from about page!
              | </body>
              |</html>
              |""".stripMargin
          )
        )
      }
    val complexPathRoute =
      path("api" / "myEndpoint") { //api/myEndpoint
        complete(StatusCodes.OK)
      }

    val dontConfuse =
      path("api/myEndpoint") {
        complete(StatusCodes.OK)
      }

    val pathEndRoute =
      pathEndOrSingleSlash { // localhost:8080 or localhost:8080/
        complete(StatusCodes.OK)
      }

    //Http().bindAndHandle(complexPathRoute, "localhost", 8080)

    /**
     * Type 2 Extraction Directives
     */

    // GET /api/item/42

    val pathExtractionRoute =
    path("api" / "item" / IntNumber) { (itemNo: Int) =>
      println(s"I have got itemNo = $itemNo")
      complete(StatusCodes.OK)

    }

    val pathMultiExtractRoute =
      path("api" / "item" / IntNumber / IntNumber) { (id, inventory) => // LongNumber HexIntNumber JavaUUID Neutral Segment etc...
        println(s"got $id and $inventory")
        complete(StatusCodes.OK)
      }
    // Http().bindAndHandle(pathMultiExtractRoute, "localhost", 8080)
    val queryParamExtractionRoute =
      path("api" / "item") {
        parameter('id.as[Int]) { (itemId: Int) =>
          println(s"extracted $itemId")
          complete(StatusCodes.OK)
        }
      }
    // Http().bindAndHandle(queryParamExtractionRoute, "localhost", 8080)

    val extractRequestRoute =
      path("controlEndpoint") {
        extractRequest { (httpRequest: HttpRequest) => // lots of other extractors
          extractLog { (log: LoggingAdapter) =>
            log.info(s"HttpRquest = $httpRequest")
            complete(StatusCodes.OK)
          }

        }
      }

    //Http().bindAndHandle(extractRequestRoute, "localhost", 8080)

    /**
     * Type 3 Composite Directives
     */

    val simpleNestedRoute =
      path("api" / "item") {
        get {
          complete(StatusCodes.OK)
        }
      }

    val compactSimpleNestedRoute = (path("api" / "item") & get) {
      complete(StatusCodes.OK)
    }

    val compactExtractRequestRoute = (path("controlEndpoint") & extractRequest & extractHost & extractLog) {
      (request, host, log) =>
        log.info(s"$host")
        log.info(s"$request $host")
        complete(StatusCodes.OK)
    }
    //    Http().bindAndHandle(compactExtractRequestRoute, "localhost", 8080)
    // /about and /aboutUs

    val repeatedRoute =
      path("about") {
        complete(StatusCodes.OK)
      } ~
        path("aboutUs") {
          complete(StatusCodes.OK)
        }

    val nonRepeatedRoute =
      (path("about") | path("aboutUs")) {
        complete(StatusCodes.OK)
      }

    // yourblog/42 AND yourblog?postId=42

    val blogByIdRoute =
      (path(IntNumber) | parameter('postId.as[Int])) { (blogId) =>
        complete(StatusCodes.OK)

      }

    /**
     * Type 4 actionable directives
     */

    val completeOkRoute = complete(StatusCodes.OK)

    val failedRoute =
      path("notSupported") {
        failWith(new RuntimeException("Unsupported!"))
      }

    val routeWithRejection =
      path("home") {
        reject  // unnecessary
      } ~
        path("index") {
          completeOkRoute
        }

    /**
     *  Exercise
     */

    val getOrPutPath =
      path("api" / "myEndpoint"){
        get{
          completeOkRoute
        } ~
        post{
          complete(StatusCodes.Forbidden)
        }
      }


  }
}
