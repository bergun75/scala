package highlevelserver

import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import akka.pattern.ask
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, StatusCodes}
import akka.stream.ActorMaterializer
import akka.util.Timeout
import highlevelserver.CitizenDb.{CreatePerson, GetPeople, GetPerson, PersonCreated}
import spray.json.DefaultJsonProtocol

import scala.util.{Failure, Success}
import scala.concurrent.duration._

/**
 *
 *- GET /api/people
 *- GET /api/people/pin
 *- GET /api/people?pin=X
 *- harder POST /api/people with a Json Denoting person
 */

case class Person(pin: Int, name: String)

object CitizenDb {

  case class CreatePerson(person: Person)

  case class PersonCreated(id: Int)

  case object GetPeople

  case class GetPerson(pin: Int)

}

class CitizenDb extends Actor with ActorLogging {
  var people = Map.empty[Int, Person]

  override def receive: Receive = {
    case CreatePerson(person) =>
      log.info(s"$person is being created")
      val newId = if (people.isEmpty) 0 else people.keys.max + 1
      people = people.updated(newId, person)
      sender() ! PersonCreated(newId)
    case GetPeople =>
      log.info(s"querying people")
      sender() ! people.values.toList
    case GetPerson(pin) =>
      log.info(s"querying person with $pin")
      sender() ! people.values.find(_.pin == pin)
  }
}

trait PersonJsonProtocol extends DefaultJsonProtocol {
  implicit val jsonFormat = jsonFormat2(Person)
  implicit val createdFormat = jsonFormat1(PersonCreated)
}


object HighLevelExercise extends PersonJsonProtocol {


  def main(args: Array[String]): Unit = {
    implicit val system = ActorSystem("HighLevelExercise")
    implicit val materializer = ActorMaterializer()
    import system.dispatcher
    import akka.http.scaladsl.server.Directives._
    import spray.json._

    val peopleDb = system.actorOf(Props[CitizenDb], "CitizenDb")
    val peopleList = List(Person(1, "ali"), Person(2, "veli"), Person(3, "49elli"))
    peopleList.foreach(person =>
      peopleDb ! CreatePerson(person)
    )

    implicit val timeout = Timeout(3 seconds)

    def toHttpEntity(entity: String) = HttpEntity(ContentTypes.`application/json`, entity)

    def personExtractDirective(pin: Int) =
      (peopleDb ? GetPerson(pin)).mapTo[Option[Person]].map(_.toJson.prettyPrint).map(toHttpEntity)

    import scala.concurrent.duration._

    val route =
      pathPrefix("api" / "people") {
        get {
          (path(IntNumber) | parameter('pin.as[Int])) { pin =>
            complete(
              personExtractDirective(pin)
            )
          } ~
            pathEndOrSingleSlash {
              complete((peopleDb ? GetPeople)
                .mapTo[List[Person]]
                .map(_.toJson.prettyPrint).map(toHttpEntity))
            }
        } ~
          (post & pathEndOrSingleSlash & extractRequest & extractLog) { (request, log) =>
            log.info(s"unmarshall $request to Person")
            val newEntityFuture = request.entity.toStrict(3 seconds)
            val newPersonFuture = newEntityFuture.map(_.data.utf8String.parseJson.convertTo[Person])
            onComplete(newPersonFuture) {
              case Success(person) =>
                log.info(s"Got $person")
                complete((peopleDb ? CreatePerson(person))
                  .mapTo[PersonCreated]
                  .map(_.toJson.prettyPrint).map(toHttpEntity))
              case Failure(ex) =>
                failWith(ex)
            }
          }
      }
    Http().bindAndHandle(route, "localhost", 8080)
  }
}
