package highlevelserver

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, StatusCodes}
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer

object HighLevelIntro {
  def main(args: Array[String]): Unit = {
    implicit val system = ActorSystem("HighLevelIntro")
    implicit val materializer = ActorMaterializer()
    import system.dispatcher

    // directives.
    import akka.http.scaladsl.server.Directives._

    val simpleRoute: Route = // RequestContext => Future[RouteResult]
      path("home") { // DIRECTIVE
        complete(StatusCodes.OK) // DIRECTIVE -> describe what happens under certain conditions
      }

    val pathGetRoute: Route =
      path("home") {
        get {
          complete(StatusCodes.OK)
        }
      }

    // chaining directives

    val chainedRoute: Route =
      path("myEndpoint") {
        get {
          complete(StatusCodes.OK)
        } ~ // <- VERY IMPORTANT
          post {
            complete(StatusCodes.Forbidden)
          }
      } ~
        path("home") {
          complete(
            HttpEntity(
              ContentTypes.`text/html(UTF-8)`,
              """
                |<html>
                | <body>
                | Hello from this exercise
                | </body>
                |</html>
                |""".stripMargin
            )
          )
        }
    Http().bindAndHandle(chainedRoute, "localhost", 8080)
  }
}
