package highlevelserver

import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.http.scaladsl.server.Directives.{complete, parameter, path, pathEndOrSingleSlash, pathPrefix}
import akka.pattern.ask
import akka.stream.ActorMaterializer
import akka.util.Timeout
import highlevelserver.GuitarDB.FindGuitarsInStock

import scala.concurrent.Future
import scala.concurrent.duration._


// step 1
import spray.json._

case class Guitar(make: String, model: String, quantity: Int = 0)

object GuitarDB {

  case class CreateGuitar(guitar: Guitar)

  case class GuitarCreated(id: Int)

  case class FindGuitar(id: Int)

  case object FindAllGuitars

  case class AddQuantity(id: Int, quantity: Int)

  case class FindGuitarsInStock(inStock: Boolean)

}


class GuitarDB extends Actor with ActorLogging {

  import GuitarDB._

  var guitars: Map[Int, Guitar] = Map()
  var currentGuitarId: Int = 0

  override def receive: Receive = {
    case FindAllGuitars =>
      log.info("Searching for all guitars")
      sender() ! guitars.values.toList

    case FindGuitar(id) =>
      log.info(s"Searching guitar by id: $id")
      sender() ! guitars.get(id)

    case CreateGuitar(guitar) =>
      log.info(s"Adding guitar $guitar with id $currentGuitarId")
      guitars = guitars + (currentGuitarId -> guitar)
      sender() ! GuitarCreated(currentGuitarId)
      currentGuitarId += 1

    case AddQuantity(id, quantity) =>
      log.info(s"Trying to add $quantity items for guitar $id")
      val guitar: Option[Guitar] = guitars.get(id)
      val newGuitar: Option[Guitar] = guitar.map {
        case Guitar(make, model, q) => Guitar(make, model, q + quantity)
      }

      newGuitar.foreach(guitar => guitars = guitars + (id -> guitar))
      sender() ! newGuitar

    case FindGuitarsInStock(inStock) =>
      log.info(s"Searching for all guitars ${if (inStock) "in" else "out of"} stock")
      if (inStock)
        sender() ! guitars.values.filter(_.quantity > 0)
      else
        sender() ! guitars.values.filter(_.quantity == 0)

  }
}

// step 2
trait GuitarStoreJsonProtocol extends DefaultJsonProtocol {
  // step 3
  implicit val guitarFormat = jsonFormat3(Guitar)
}


object HighLevelExample extends GuitarStoreJsonProtocol {
  implicit val system = ActorSystem("HighLevelExample")
  implicit val materializer = ActorMaterializer()
  import system.dispatcher

  /*
  setup
 */
  val guitarDb = system.actorOf(Props[GuitarDB], "LowLevelGuitarDB")
  val guitarList = List(
    Guitar("Fender", "Stratocaster"),
    Guitar("Gibson", "Les Paul"),
    Guitar("Martin", "LX1")
  )
  import highlevelserver.GuitarDB.CreateGuitar

  guitarList.foreach { guitar =>
    guitarDb ! CreateGuitar(guitar)
  }

  import akka.http.scaladsl.server.Directives._

  // GET /api/guitar fetches ALL guitars
  // GET /api/guitar?id=x
  // GET /api/guitar/x
  // GET /api/guitar/inventory?inStock=true/false

  import GuitarDB._
  implicit val timeOut = Timeout(2 seconds)

  def toHttpEntity(payload: String) = HttpEntity(ContentTypes.`application/json`, payload)

  def guitarExtractDirective(guitarId: Int) = {
    val guitarFuture = (guitarDb ? FindGuitar(guitarId)).mapTo[Option[Guitar]].map(_.toJson.prettyPrint)
      .map(toHttpEntity)
    complete(guitarFuture)
  }

  val guitarServerRoute =
    path("api" / "guitar") {
      parameter('id.as[Int]) { guitarId => //ALWAYS declare the more specific
        get {
          guitarExtractDirective(guitarId)
        }
      } ~
        get {
          val guitarsFuture = (guitarDb ? FindAllGuitars).mapTo[List[Guitar]].map(_.toJson.prettyPrint).map(toHttpEntity)
          complete(guitarsFuture)
        }
    } ~
      path("api" / "guitar" / IntNumber) { guitarId =>
        get {
          guitarExtractDirective(guitarId)
        }
      } ~
      path("api" / "guitar" / "inventory") {
        get {
          parameter('inStock.as[Boolean]) { inStock =>
            val guitarsFuture = (guitarDb ? FindGuitarsInStock(inStock)).mapTo[List[Guitar]].map(_.toJson.prettyPrint)
              .map(toHttpEntity)
            complete(guitarsFuture)
          }
        }
      }


  val simplifiedGuitarServerRoute =
    (pathPrefix("api" / "guitar") & get) {
      path("inventory") {
        parameter('inStock.as[Boolean]) { inStock =>
          complete((guitarDb ? FindGuitarsInStock(inStock)).mapTo[List[Guitar]]
            .map(_.toJson.prettyPrint)
            .map(toHttpEntity))
        }
      } ~
        (path(IntNumber) | parameter('id.as[Int])) { guitarId =>
          guitarExtractDirective(guitarId)
        } ~
        pathEndOrSingleSlash {
          complete((guitarDb ? FindAllGuitars).mapTo[List[Guitar]]
            .map(_.toJson.prettyPrint)
            .map(toHttpEntity))
        }

    }


  def main(args: Array[String]): Unit = {




    Http().bindAndHandle(simplifiedGuitarServerRoute, "localhost", 8080)

  }
}
