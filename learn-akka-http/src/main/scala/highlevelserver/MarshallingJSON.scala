package highlevelserver

import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.{Rejection, RejectionHandler}
import akka.pattern.ask
import akka.stream.ActorMaterializer
import akka.util.Timeout
import highlevelserver.GameAreaMap._
import spray.json._

import scala.concurrent.duration._

case class Player(nickName: String, characterClass: String, level: Int)

object GameAreaMap {

  case object GetAllPlayers

  case class GetPlayer(nickName: String)

  case class GetAllPlayersByClass(characterClass: String)

  case class AddPlayer(player: Player)

  case class RemovePlayer(player: PlayerToRemove)

  case class OperationSuccess(playerNickName: String)

  case class PlayerToRemove(nickName: String)

}

class GameAreaMap extends Actor with ActorLogging {

  var players = Map.empty[String, Player]

  override def receive: Receive = {
    case GetAllPlayers =>
      log.info("Getting all players")
      sender() ! players.values.toList
    case GetPlayer(nickName) =>
      log.info(s"Getting player with nickname $nickName")
      sender() ! players.get(nickName)
    case GetAllPlayersByClass(className) =>
      log.info(s"Getting player with characterClass $className")
      sender() ! players.values.toList.filter(_.characterClass == className)
    case AddPlayer(player) =>
      log.info(s"Adding player $player")
      players = players + (player.nickName -> player)
      sender() ! OperationSuccess(player.nickName)
    case RemovePlayer(player) =>
      log.info(s"Trying to remove $player")
      players = players - player.nickName
      sender() ! OperationSuccess(player.nickName)
  }
}

trait PlayerJsonProtocol extends DefaultJsonProtocol {
  implicit val playerFormat = jsonFormat3(Player)
  implicit val playerToRemove = jsonFormat1(PlayerToRemove)
}

object MarshallingJSON extends PlayerJsonProtocol with SprayJsonSupport {
  implicit val system = ActorSystem("MarshallingJSON")
  implicit val materializer = ActorMaterializer()
  import akka.http.scaladsl.server.Directives._
  import system.dispatcher

  val gameMapDb = system.actorOf(Props[GameAreaMap], "gameAreaMap")
  val playerList = List(Player("bergun75", "Warrior", 70), Player("roland_", "Elf", 67), Player("daniel", "Wizard", 30))
  playerList.foreach(player => gameMapDb ! AddPlayer(player))

  /**
   * - GET /api/player
   * - GET /api/player/(nickname)
   * - GET /api/player?nickname=X
   * - GET /api/player/class/(charClass)
   * - POST /api/player with Json payload
   * - DELETE /api/player with Json payload
   */

  val badRequestHandler: RejectionHandler = { rejections: Seq[Rejection] =>
    println(s"Have encountered ${rejections.mkString(";")}")
    Some(complete(StatusCodes.BadRequest))
  }

  val forbiddenRequestHandler: RejectionHandler = { rejections: Seq[Rejection] =>
    println(s"Have encountered ${rejections.mkString(";")}")
    Some(complete(StatusCodes.Forbidden))
  }

  implicit val timeout = Timeout(3 seconds)
  val route =
    pathPrefix("api" / "player") {
      handleRejections(badRequestHandler) {
        get {
          path("class" / Segment) { charClass =>
            val players = (gameMapDb ? GetAllPlayersByClass(charClass)).mapTo[List[Player]]
            complete(players)
          } ~
            (path(Segment) | parameter('nickname)) { nickName =>
              val player = (gameMapDb ? GetPlayer(nickName)).mapTo[Option[Player]]
              complete(player)
            } ~
            pathEndOrSingleSlash {
              val players = (gameMapDb ? GetAllPlayers).mapTo[List[Player]]
              complete(players)
            }
        } ~
          post {
            handleRejections(forbiddenRequestHandler) {
              parameter('className) { className => // override className with parameter
                entity(as[Player]) { player => // as = implicitly[FromRequestUnmarshaller[Player]]
                  complete((gameMapDb ? AddPlayer(player.copy(characterClass = className))).map(_ => StatusCodes.Created))
                }
              } ~
                pathEndOrSingleSlash {
                  entity(as[Player]) { player => // as = implicitly[FromRequestUnmarshaller[Player]]
                    complete((gameMapDb ? AddPlayer(player)).map(_ => StatusCodes.Created))
                  }
                }
            }
          } ~
          delete {
            entity(as[PlayerToRemove]) { player =>
              complete((gameMapDb ? RemovePlayer(player)).map(_ => StatusCodes.OK))
            }
          }
      }
    }

  def main(args: Array[String]): Unit = {


    Http().bindAndHandle(route, "localhost", 8080)

  }
}
