package com.pairwiseltd.fruitshop.checkout

import com.pairwiseltd.fruitshop.offer.Offers
import com.pairwiseltd.fruitshop.model.ShoppingCart
import java.util.Currency
import java.util.Locale
import com.pairwiseltd.fruitshop.model.Money
import com.pairwiseltd.fruitshop.model.TaxChain

trait CheckoutMagnet[T] {
  def apply(): T
}

object CheckoutMagnet {
  implicit def fromShoppingCart(cart: ShoppingCart) = new CheckoutMagnet[BigDecimal] {
    def apply(): BigDecimal =
      cart.foldLeft(BigDecimal(0.0))((acc, item) => acc + item.money.amount)
  }
  implicit def fromShoppingCartAndOffer(tuple: (ShoppingCart, Offers, Currency)) =
    new CheckoutMagnet[Money] {
      def apply(): Money = {
        val (cart, offers, desiredCur) = tuple
        val groupedCart = cart.groupBy(item => item.itemName)
        val groupedCartWithOffer =
          groupedCart map (entry =>
            {
              val (itemName, itemSeq) = entry
              (itemName,
                offers.getAfterOfferCountDef(itemName)(itemSeq.size),
                itemSeq(0).money.amount,
                itemSeq(0).money.currency)
            })
        groupedCartWithOffer.foldLeft(Money(BigDecimal(0.0))(desiredCur))((acc, entry) => {
          val (_, count, price, currency) = entry
          // TODO handle any currrency conversions if required here
          // assume all currencys are same at the moment        
          Money(acc.amount + (count * price))(acc.currency)
        })
      }
    }

}

trait CheckoutComponent {
  val checkout: Checkout
  class Checkout {
    /**
     * Calculate items added into shopping Cart
     *
     * @param cart ShoppingCart a sequence of shopping items
     *
     * @return T sum of prices of items in the cart
     */
    def calculateTotal[T](magnet: CheckoutMagnet[T]): T = magnet()
  }
}
