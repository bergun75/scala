package com.pairwiseltd.fruitshop.model

import java.util.Currency

import scala.BigDecimal

case class ShoppingItem(itemName: String,
                        money: Money,
                        tags: Set[String] = Set()) {
  require(money.amount >= BigDecimal(0.0))
}

case class Money(amount: BigDecimal)(implicit val currency: Currency) {
  require(amount >= BigDecimal(0.0))
  def *(rate: BigDecimal): Money = Money((amount * rate).setScale(2, BigDecimal.RoundingMode.HALF_UP))(currency)
  def +(other: Money): Money = Money(amount + other.amount)(currency) // Ignore currency conversion
}

case class Tax(name: String, rate: BigDecimal, isAppliedToGross: Boolean) {
  require(rate >= BigDecimal(0.0) && rate <= BigDecimal(1.0), "rate is a percentage not exceeding 1.00")
}


