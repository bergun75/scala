package com.pairwiseltd.fruitshop

import java.util.Locale
import java.util.Currency
import com.pairwiseltd.fruitshop.model.Money
import com.pairwiseltd.fruitshop.model.ShoppingItem
import com.pairwiseltd.fruitshop.model.Tax



package object model {
  
  val APPLE = "apple"
  val ORANGE = "orange"
  implicit val currency = Currency.getInstance(Locale.UK)
  val APPLE_PRICE = Money(BigDecimal(0.60))
  val ORANGE_PRICE = Money(BigDecimal(0.25))
  
  val VAT1_TAX = "vat"
  val SUGAR_TAX = "sugar"
  val VAT1_TAX_RATE = BigDecimal(0.2)
  val SUGAR_TAX_RATE = BigDecimal(0.01)
  
  type ShoppingCart = Seq[ShoppingItem]
  type TaxChain = Seq[Tax]
}