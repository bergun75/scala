package com.pairwiseltd.fruitshop

import java.util.Currency
import java.util.Locale

import scala.annotation.tailrec

import com.pairwiseltd.fruitshop.checkout.CheckoutMagnet.fromShoppingCart
import com.pairwiseltd.fruitshop.checkout.CheckoutMagnet.fromShoppingCartAndOffer
import com.pairwiseltd.fruitshop.model.ShoppingItem
import com.pairwiseltd.fruitshop.offer.PrimitiveOffersDB
import com.pairwiseltd.fruitshop.tax.TaxMagnet.calculateWithItemAndTaxChain

import common.NEWLINE
import common.timed

object ShopRunner{  
  val DEFAULT_ITEM_COUNT = 100
  val AT_LEAST_ONE_ELEMENT_MSG = "Sequence must contain at least 1 member"
  val USAGE_MSG = NEWLINE +
    NEWLINE + "runMain com.pairwiseltd.fruitshop.ShopRunner [num_of_random_items]" +
    NEWLINE

  def main(args: Array[String]) {
    val numItems = if (args.size == 1) args(0).toInt else DEFAULT_ITEM_COUNT
    implicit val desiredCurrency = Currency.getInstance(Locale.UK)
    def generateItemList(num: Int): List[ShoppingItem] = {
      require(num > 0, AT_LEAST_ONE_ELEMENT_MSG)
      val allItems = FruitShop.database.findAllProducts
      val taxChain = FruitShop.database.findTaxChainRules
      val taxedItems = allItems map (item => FruitShop.taxprocessor.applyTax(item, taxChain))      
      val prg = scala.util.Random
      @tailrec
      def loop(idx: Int, items: List[ShoppingItem]): List[ShoppingItem] =
        {
          if (idx == num) items
          else loop(idx + 1, taxedItems(prg.nextInt(taxedItems.size)) :: items)
        }
      loop(0, Nil)
    }
    val shoppingCart = generateItemList(numItems)
    println(s"ShopRunner running with $numItems items")
    //System.err.println(shoppingCart)
    println("PrimitiveFruitShop checkout total= " +
      timed { FruitShop.checkout.calculateTotal(shoppingCart) })    
    println("AdvancedFruitShop checkout total= " +
      timed { FruitShop.checkout.calculateTotal(shoppingCart, PrimitiveOffersDB, desiredCurrency)})
  }
}

