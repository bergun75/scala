package com.pairwiseltd.fruitshop.dao

import com.pairwiseltd.fruitshop.model.APPLE
import com.pairwiseltd.fruitshop.model.APPLE_PRICE
import com.pairwiseltd.fruitshop.model.ORANGE
import com.pairwiseltd.fruitshop.model.ORANGE_PRICE
import com.pairwiseltd.fruitshop.model.SUGAR_TAX
import com.pairwiseltd.fruitshop.model.SUGAR_TAX_RATE
import com.pairwiseltd.fruitshop.model.VAT1_TAX
import com.pairwiseltd.fruitshop.model.VAT1_TAX_RATE
import com.pairwiseltd.fruitshop.model.ShoppingItem
import com.pairwiseltd.fruitshop.model.Tax
import com.pairwiseltd.fruitshop.model.TaxChain

trait DaoComponent {
  val database: ProductsDao
  trait ProductsDao {
    /**
     * Finds item from the product database with its name.
     *
     * @param itemName String with the item is registered in the DB
     *
     * @return Option[ShoppingItem] if found Some(x) if not found None
     */
    def find(itemName: String): Option[ShoppingItem]

    /**
     * Finds all items from the product database.
     *
     * @return Seq[ShoppingItem] sequence of all items
     */
    def findAllProducts(): Seq[ShoppingItem]
  }

  trait TaxDao {
    def findTaxChainRules(): TaxChain
  }
}

trait PrimitiveDaoComponentImpl extends DaoComponent {
  val productMap = Map(APPLE -> ShoppingItem(APPLE, APPLE_PRICE, Set(VAT1_TAX, SUGAR_TAX)),
    ORANGE -> ShoppingItem(ORANGE, ORANGE_PRICE, Set(VAT1_TAX)))
  val unfairTaxChain: TaxChain = Seq(Tax(VAT1_TAX, VAT1_TAX_RATE, false),
    Tax(SUGAR_TAX, SUGAR_TAX_RATE, true))
  class PrimitiveDao extends ProductsDao
      with TaxDao {
    override def find(itemName: String): Option[ShoppingItem] = {
      productMap.get(itemName)
    }

    override def findAllProducts(): Seq[ShoppingItem] = productMap.values.toSeq

    override def findTaxChainRules: TaxChain = unfairTaxChain
  }
}