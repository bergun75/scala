package com.pairwiseltd.fruitshop

import com.pairwiseltd.fruitshop.checkout.CheckoutComponent
import com.pairwiseltd.fruitshop.dao.PrimitiveDaoComponentImpl
import com.pairwiseltd.fruitshop.tax.TaxComponent
import com.pairwiseltd.fruitshop.tax.TaxComponent

object FruitShop extends CheckoutComponent
    with PrimitiveDaoComponentImpl 
    with TaxComponent{
  val taxprocessor = new TaxProcessor
  val checkout = new Checkout
  val database = new PrimitiveDao
}