package com.pairwiseltd.fruitshop.tax

import com.pairwiseltd.fruitshop.model.ShoppingItem
import com.pairwiseltd.fruitshop.model.TaxChain

trait TaxMagnet[T] {
  def apply(): T
}

object TaxMagnet {
  implicit def calculateWithItemAndTaxChain(tuple: (ShoppingItem, TaxChain)) = new TaxMagnet[ShoppingItem] {
    override def apply(): ShoppingItem = {
      val (item, chain) = tuple
      val money = item.money
      // TODO ignoring Rounding in the first place
      val accumulated = chain.foldLeft(money) {
        case (acc, tax) => {
          if (item.tags.contains(tax.name)) {
            if (tax.isAppliedToGross) acc + (acc * (tax.rate))
            else acc + (item.money * (tax.rate))
          } else acc
        }
      }
      ShoppingItem(item.itemName, accumulated, item.tags)
    }
  }
}
trait TaxComponent {
  val taxprocessor: TaxProcessor

  class TaxProcessor {
    def applyTax[T](magnet: TaxMagnet[T]): T = magnet()
  }
}