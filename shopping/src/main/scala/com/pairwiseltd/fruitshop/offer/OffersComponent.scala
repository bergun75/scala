package com.pairwiseltd.fruitshop.offer

import com.pairwiseltd.fruitshop.model.APPLE
import com.pairwiseltd.fruitshop.model.ORANGE

trait Offers {
  /**
   * High order function that returns the function that takes
   * "number of items" as input and calculates how many of them
   * is to be charged
   *
   * @param itemName String name of the item as string to pick
   * the related offer's Int => Int anonymous function to be used.
   *
   * @return Int => Int returns the function that takes
   * "number of items" as input and calculates how many of them
   *  is to be charged
   */
  def getAfterOfferCountDef(itemName: String): Int => Int
}

object PrimitiveOffersDB extends Offers {
  def getAfterOfferCountDef(itemName: String): Int ⇒ Int = {
    itemName match {
      case APPLE  => (x => x - (x / 2))
      case ORANGE => (x => x - (x / 3))
      case _      => (x => x)
    }
  }
}


