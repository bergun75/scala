import com.pairwiseltd.fruitshop.model.Money
import java.util.Currency
import java.util.Locale

package object common {

  val NEWLINE = "\n"

  // wanted to control literals in a central place. These would be actually
  // part of a more advanced database implementations with methods such
  // as list me all available products etc..
  
  /**
   * Executes its function argument with elapsed time information
   *
   *  @param block function that will be executed within the timed block
   *  @return R original return type of the block function
   */
  def timed[R](block: => R): R = {
    val t0 = System.nanoTime()
    val result = block // call-by-name
    val t1 = System.nanoTime()
    println("Elapsed time: " + (t1 - t0) / 1000000 + "ms")
    result
  }
}