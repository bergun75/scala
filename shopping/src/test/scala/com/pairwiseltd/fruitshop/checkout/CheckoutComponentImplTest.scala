package com.pairwiseltd.fruitshop.checkout

import java.util.Currency
import java.util.Locale

import scala.BigDecimal

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

import com.pairwiseltd.fruitshop.checkout.CheckoutMagnet.fromShoppingCart
import com.pairwiseltd.fruitshop.dao.PrimitiveDaoComponentImpl
import com.pairwiseltd.fruitshop.model.APPLE
import com.pairwiseltd.fruitshop.model.ORANGE
import com.pairwiseltd.fruitshop.model.Money
import com.pairwiseltd.fruitshop.offer.PrimitiveOffersDB

@RunWith(classOf[JUnitRunner])
class CheckoutComponentImplTest extends FunSuite {

  trait TestData extends PrimitiveDaoComponentImpl
      with CheckoutComponent {
    val database = new PrimitiveDao
    val checkout = new Checkout
    val offers = PrimitiveOffersDB
    implicit val desiredCurrency = Currency.getInstance(Locale.UK)
    val apple = database.find(APPLE).get
    val orange = database.find(ORANGE).get
    val emptyShoppingCart = List()
    val shoppingCart1 = List(apple, apple, orange, apple, orange, orange, apple, apple)
    val shoppingCart2 = List(apple, orange, orange, apple, orange, orange, apple, orange)

  }

  test("shoppingCart1 5 apples and 3 oranges must cost BigDecimal(3.75)") {
    new TestData {
      assert(checkout.calculateTotal(shoppingCart1) == BigDecimal(3.75))
    }
  }

  test("shoppingCart1 3 apples and 5 oranges must cost BigDecimal(3.05)") {
    new TestData {
      assert(checkout.calculateTotal(shoppingCart2) == BigDecimal(3.05))
    }
  }

  test("emptyShoppingCart must cost BigDecimal(0.0)") {
    new TestData {
      assert(checkout.calculateTotal(emptyShoppingCart) == BigDecimal(0.0))
    }
  }

  test("shoppingCart1 5 apples and 3 oranges must cost Money(BigDecimal(2.30), GBP) with offers") {
    new TestData {
      assert(checkout.calculateTotal(shoppingCart1, offers, desiredCurrency) == Money(BigDecimal(2.30)))
    }
  }

  test("shoppingCart1 3 apples and 5 oranges must cost Money(BigDecimal(2.20), GBP) with offers") {
    new TestData {
      assert(checkout.calculateTotal(shoppingCart2, offers, desiredCurrency) == Money(BigDecimal(2.20)))
    }
  }

  test("emptyShoppingCart must cost Money(BigDecimal(0.0), GBP)") {
    new TestData {
      assert(checkout.calculateTotal(emptyShoppingCart, offers, desiredCurrency) == Money(BigDecimal(0.0)))
    }
  }

  // could do property tests with scala check though that would be pretty much testing
  // BigDecimal addition operation no precision loss which is already guranteed with
  // BigDecimal
  // TODO if any division or multiplication operations involved precision rounding
  // property tests can be provided.
}
