package com.pairwiseltd.fruitshop.tax

import java.util.Currency
import java.util.Locale

import scala.BigDecimal

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

import com.pairwiseltd.fruitshop.dao.PrimitiveDaoComponentImpl
import com.pairwiseltd.fruitshop.model.APPLE
import com.pairwiseltd.fruitshop.model.ORANGE
import com.pairwiseltd.fruitshop.model.ShoppingItem
import com.pairwiseltd.fruitshop.model.Tax
import com.pairwiseltd.fruitshop.model.TaxChain
import com.pairwiseltd.fruitshop.model.Money

@RunWith(classOf[JUnitRunner])
class TaxCalculatorTax extends FunSuite {

  trait TestData extends TaxComponent
      with PrimitiveDaoComponentImpl {
    val taxprocessor = new TaxProcessor
    val database = new PrimitiveDao
    implicit val defaultCurrency = Currency.getInstance(Locale.UK)
    val apple = database.find(APPLE).get
    val orange = database.find(ORANGE).get

    val fairTaxChain: TaxChain = Seq(Tax("vat", BigDecimal(0.2), false),
      Tax("sugar", BigDecimal(0.008), false))
    val unfairTaxChain2: TaxChain = Seq(Tax("vat", BigDecimal(0.2), false),
      Tax("sugar", BigDecimal(0.008), true))
  }

  test("calculate after Tax amount of apple after fair taxChainApplied") {
    new TestData {
      assert(taxprocessor.applyTax(apple, fairTaxChain) ==
        ShoppingItem(apple.itemName, Money(BigDecimal(0.72)), apple.tags))
    }
  }

  test("calculate after Tax amount of apple after unfair taxChainApplied") {
    new TestData {
      assert(taxprocessor.applyTax(apple, unfairTaxChain2) ==
        ShoppingItem(apple.itemName, Money(BigDecimal(0.73)), apple.tags))
    }
  }

  test("calculate after Tax amount of orange after fair taxChainApplied") {
    new TestData {
      assert(taxprocessor.applyTax(orange, fairTaxChain) ==
        ShoppingItem(orange.itemName, Money(BigDecimal(0.30)), orange.tags))
    }
  }

  test("calculate after Tax amount of orange after unfair taxChainApplied") {
    new TestData {
      assert(taxprocessor.applyTax(orange, unfairTaxChain2) ==
        ShoppingItem(orange.itemName, Money(BigDecimal(0.30)), orange.tags))
    }
  }

  // could do property tests with scala check though that would be pretty much testing
  // BigDecimal addition operation no precision loss which is already guranteed with
  // BigDecimal
  // TODO if any division or multiplication operations involved precision rounding
  // property tests can be provided.
}

