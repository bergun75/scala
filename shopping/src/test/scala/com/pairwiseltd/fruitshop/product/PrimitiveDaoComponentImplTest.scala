package com.pairwiseltd.fruitshop.dao

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

import com.pairwiseltd.fruitshop.model.APPLE
import com.pairwiseltd.fruitshop.model.APPLE_PRICE
import com.pairwiseltd.fruitshop.model.ORANGE
import com.pairwiseltd.fruitshop.model.ORANGE_PRICE


@RunWith(classOf[JUnitRunner])
class PrimitiveDaoComponentImplTest extends FunSuite {
  
  trait TestData extends PrimitiveDaoComponentImpl {
    val BANANA = "banana"
    val database = new PrimitiveDao
  }

  test("apple is found in the primitive product database") {
    new TestData {
      assert(database.find(APPLE) match {
        case Some(x) => x.itemName == APPLE && x.money == APPLE_PRICE
        case None    => false
      })
    }
  }

  test("orange is found in the primitive product database") {
    new TestData {
      assert(database.find(ORANGE) match {
        case Some(x) => x.itemName == ORANGE && x.money == ORANGE_PRICE
        case None    => false
      })
    }
  }

  test("banana is not found in the primitive product database") {
    new TestData {
      assert(database.find(BANANA) match {
        case Some(x) => false
        case None    => true
      })
    }
  }
}
