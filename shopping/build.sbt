lazy val commonSettings = Seq(
  name         := "fruitshop",
  organization := "pairwiseltd",
  version := "0.1.0-SNAPSHOT",
  scalaVersion := "2.11.8",  
  mainClass := Some("com.pairwiseltd.fruitshop.ShopRunner")
)

lazy val root = (project in file("."))
  .settings(
    commonSettings,        
    libraryDependencies += "junit" % "junit" % "4.10" % "test",
    libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.1",
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % "test"
  )
