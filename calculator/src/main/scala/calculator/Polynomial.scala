package calculator

object Polynomial {
  final val POWER_TWO = 2.0
  val delta = Var(0.0)
  val solutions = Var(Set[Double]())
  def computeDelta(a: Signal[Double], b: Signal[Double],
                   c: Signal[Double]): Signal[Double] = {
    delta() = deltaFunction(a(), b(), c())
    delta
  }

  private def deltaFunction(a: Double, b: Double, c: Double): Double = {
    Math.pow(b, POWER_TWO) - (4 * a * c)
  }

  def computeSolutions(a: Signal[Double], b: Signal[Double],
                       c: Signal[Double], delta: Signal[Double]): Signal[Set[Double]] = {
    solutions() = solution(a(), b(), c(), delta())
    solutions
  }

  private def solution(a: Double, b: Double, c: Double, delta: Double): Set[Double] = {
    if (delta < 0.0) Set()
    else if (delta == 0) Set(-b / (2 * a))
    else {
      val sqrtDelta = Math.sqrt(delta)
      val deltaList = List(sqrtDelta, -sqrtDelta)
      val k = for {
        d <- deltaList
      } yield (-b + d) / (2 * a)
      k.toSet
    }
  }
}
