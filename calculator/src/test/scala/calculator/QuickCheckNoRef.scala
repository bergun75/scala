package calculator

import common._
import org.scalacheck._
import org.scalacheck.Arbitrary._
import org.scalacheck.Gen._
import org.scalacheck.Prop._

class QuickCheckCalculatorNoRef extends Properties("Calculator") {

  lazy val genExpr: Gen[Map[String, Var[Expr]]] = for {
    dExpr1 <- choose(-10000, 10000)
    dExpr2 <- choose(-10000, 10000)
    expr: Expr = Literal(dExpr1)
    expr2: Expr = Plus(Ref("a"), Literal(dExpr2))
    m <- Map("a" -> Var(expr), "b" -> Var(expr2))
  } yield m

  implicit lazy val arbExpr: Arbitrary[Map[String, Var[Expr]]] = Arbitrary(genExpr)

//  property("results will be calculated") = forAll { (exprSig: Map[String, Var[Expr]]) =>
//    val res = Calculator.computeValues(exprSig)
//    val a = res("a")()
//    val b = res("b")()
//    println(a)
//    println(b)
//    println()
//    res("a")() = a + 1
//    println(res("a")())
//    println(res("b")())
//    println()
//    res("b")() == b
//  }

}
