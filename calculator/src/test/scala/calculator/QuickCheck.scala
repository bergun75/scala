package calculator

import common._
import org.scalacheck._
import org.scalacheck.Arbitrary._
import org.scalacheck.Gen._
import org.scalacheck.Gen.sized
import org.scalacheck.Prop._

class QuickCheckCalculator extends Properties("Calculator") {
  
  private def genMap[K,V](genElem: Gen[(K,V)]): Gen[Map[K, V]] = {
  sized { sz: Int =>
    for {      
      list <- Gen.mapOfN(20, genElem)
    } yield list
  }
}

  lazy val genExpr: Gen[(String, Signal[Expr])] = for {
    s <- Gen.choose('a', 'j')
    dExpr1 <- arbitrary[Double]
    dExpr2 <- arbitrary[Double]
    sExpr1 <- oneOf("a", "b", "c", "d", "e", "f", "g", "h", "i", "j") suchThat { x => x != s }
    sExpr2 <- oneOf("a", "b", "c", "d", "e", "f", "g", "h", "i", "j") suchThat { x => x != s }    
    b <- oneOf(Literal(dExpr1),
      Ref(sExpr1),
      Plus(oneOf(Literal(dExpr1), Ref(sExpr1)).sample.get, oneOf(Literal(dExpr2), Ref(sExpr2)).sample.get),
      Minus(oneOf(Literal(dExpr1), Ref(sExpr1)).sample.get, oneOf(Literal(dExpr2), Ref(sExpr2)).sample.get),
      Times(oneOf(Literal(dExpr1), Ref(sExpr1)).sample.get, oneOf(Literal(dExpr2), Ref(sExpr2)).sample.get),
      Divide(oneOf(Literal(dExpr1), Ref(sExpr1)).sample.get, oneOf(Literal(dExpr2), Ref(sExpr2)).sample.get))
  } yield (s.toString(), Signal(b))

  
  implicit lazy val arbExpr: Arbitrary[(String, Signal[Expr])] = Arbitrary(genExpr)
  
  implicit lazy val arbExprMap: Arbitrary[Map[String, Signal[Expr]]] = Arbitrary(genMap(genExpr))

    property("results will be calculated") = forAll { (exprSig: Map[String, Signal[Expr]]) =>
    val res = Calculator.computeValues(exprSig)
    !res.isEmpty
  }  
  

}
