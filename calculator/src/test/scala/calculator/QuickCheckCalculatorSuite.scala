package calculator

import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import org.scalatest.prop.Checkers
import org.scalacheck.Arbitrary._
import org.scalacheck.Prop
import org.scalacheck.Prop._

import org.scalatest.exceptions.TestFailedException


@RunWith(classOf[JUnitRunner])
class QuickCheckCalculatorSuite extends FunSuite with Checkers {
  def checkBogus(p: Prop) {
    var ok = false
    try {
      check(p)
    } catch {
      case e: TestFailedException =>
        ok = true
    }
    assert(ok, "A bogus heap should NOT satisfy all properties. Try to find the bug!")
  }

  test("CalculatorNoRef satisfies properties.") {
    check(new QuickCheckCalculatorNoRef)
  }
  
  test("Calculator satisfies properties.") {
    check(new QuickCheckCalculator)
  }

}
