package timeusage

import org.apache.spark.annotation.InterfaceStability
import org.apache.spark.sql.Row
import org.apache.spark.sql.types.DoubleType
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType
import org.scalatest.BeforeAndAfterAll
import org.scalatest.Finders
import org.scalatest.Matchers
import org.scalatest.WordSpec
import org.scalatest.prop.TableDrivenPropertyChecks

class TimeUsageWithSparkSpecs extends WordSpec
    with Matchers
    with TableDrivenPropertyChecks
    with BeforeAndAfterAll
    with TestColumnsReal {

  override def afterAll {

  }

  val TEST_RESOURCE = "timeusage/atussum_test.csv"
  val EPS = 1e-3 // Our epsilon

  "TimeUsage" when {
    "timeUsageSummary method" should {
      "return expected Dataframe" in {
        val expectedSchema = StructType(Array(
          StructField("working", StringType, false),
          StructField("sex", StringType, false),
          StructField("age", StringType, false),
          StructField("primaryNeeds", DoubleType, true),
          StructField("work", DoubleType, true),
          StructField("other", DoubleType, true)))
        val expectedData = Array(
          Row("working", "female", "active", 13.833333333333334, 0.0, 10.166666666666666),
          Row("not working", "female", "young", 13.083333333333334, 2.0, 8.916666666666666),
          Row("working", "male", "active", 11.783333333333333, 8.583333333333334, 3.6333333333333333))

        val (columns, initDf) = TimeUsage.read("/timeusage/atussum_test.csv")
        val (primaryNeedsColumns, workColumns, otherColumns) = TimeUsage.classifiedColumns(columns)
        val summaryDf = TimeUsage.timeUsageSummary(primaryNeedsColumns, workColumns, otherColumns, initDf)

        val rowResult = summaryDf.collect()
        assert(rowResult.size == expectedData.size)
        assert(expectedSchema == summaryDf.schema, "expectedSchema == summaryDf.schema")
        val resComp = rowResult zip expectedData
        val areAllEqual = resComp forall {
          case (actual, expected) => {
            if ((actual.getString(0) === expected.getString(0)) &&
              (actual.getString(1) === expected.getString(1)) &&
              (actual.getString(2) === expected.getString(2)) &&
              (actual.getDouble(3) === (expected.getDouble(3) +- EPS)) &&
              (actual.getDouble(4) === (expected.getDouble(4) +- EPS)) &&
              (actual.getDouble(5) === (expected.getDouble(5) +- EPS))) true
            else false
          }
        }

        assert(areAllEqual, s"expectedData == rowResult")
      }
    }
  }

}
