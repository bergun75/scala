package timeusage

import org.apache.spark.annotation.InterfaceStability
import org.apache.spark.sql.types.DoubleType
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType
import org.scalatest.Finders
import org.scalatest.Matchers
import org.scalatest.WordSpec
import org.scalatest.prop.TableDrivenPropertyChecks

trait TestData extends TableDrivenPropertyChecks {
  val examples =
    Table(
      ("columnNames", "expectedStructType", "rowData"),
      (List(TimeUsage.NAME_COLUMN, "work", "eat", "bath", "gym"),
        StructType(Array(StructField(TimeUsage.NAME_COLUMN, StringType, false),
          StructField("work", DoubleType, false),
          StructField("eat", DoubleType, false),
          StructField("bath", DoubleType, false),
          StructField("gym", DoubleType, false))),
        List("\"234342342\"", "1.0", "2.0")),
      (List("foo", "bar", "baz", "bath", "gym"),
        StructType(Array(StructField("foo", StringType, false),
          StructField("bar", DoubleType, false),
          StructField("baz", DoubleType, false),
          StructField("bath", DoubleType, false),
          StructField("gym", DoubleType, false))),
        List("foo", "1.0", "2.0")))
}

trait TestDataReal
    extends TableDrivenPropertyChecks
    with TestColumnsReal {
  val realExamples = Table(
    ("realColumnNames", "primaryNeedsColumns", "workingActivitiesColumns", "otherActivitiesColumns"),
    (columns,
      pNColumns,
      wAColumns,
      oAColumns))
}

class TimeUsageSpecs extends WordSpec
    with Matchers
    with TestData
    with TestDataReal {

  forAll(examples) { (columnNames, expectedStructType, rowData) =>
    s"TimeUsage $columnNames" when {
      "dfSchema method" should {
        "return appropriate StructType provided with columnNames" in {
          assert(TimeUsage.dfSchema(columnNames) == expectedStructType)
        }
      }
      s"row method $rowData" should {
        "create desired row with rowData" in {
          val res = TimeUsage.row(rowData)
          assert(res.get(0).isInstanceOf[String])
          assert(res.get(1).isInstanceOf[Double])
          assert(res.get(2).isInstanceOf[Double])
        }
      }
    }
  }

  forAll(realExamples) { (realColumnNames, primaryNeedsColumns, workingActivitiesColumns, otherActivitiesColumns) =>
    "TimeUsage" when {
      "classifiedColumns method" should {
        "return expected columns" in {
          val (p, w, o) = TimeUsage.classifiedColumns(realColumnNames)

          assert(p == primaryNeedsColumns, s"primaryNeedsColumns should have no diff=${p.diff(primaryNeedsColumns)}")
          assert(w == workingActivitiesColumns, s"workingActivitiesColumns should have no diff=${w.diff(workingActivitiesColumns)}")
          assert(o == otherActivitiesColumns, s"otherActivitiesColumns should have no diff=${o.diff(otherActivitiesColumns)}")
        }
      }
    }

  }
}
