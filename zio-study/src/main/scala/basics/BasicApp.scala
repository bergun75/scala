package basics

import zio._
import zio.console._
object BasicApp extends App {
  val myAppLogic =
    for {
      _    <- putStrLn("Hello! What is your name?")
      name <- getStrLn
      _    <- putStrLn(s"Hello, ${name}, welcome to ZIO!")
    } yield ()

  val surnameLogic =
    for {
      _    <- putStrLn("Hello! What is your surname?")
      surname <- getStrLn
      _    <- putStrLn(s"Hello, Mr ${surname}, welcome to ZIO!")
    } yield ()

  override def run(args: List[String]): URIO[zio.ZEnv, ExitCode] =
    myAppLogic.exitCode
}
