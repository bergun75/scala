package preserveinfo

import zio._

object PreserveInfo extends App{
  def readFile(path: String): ZIO[Any, IOError, String] = ???
  def parseConfig(s: String): ZIO[Any, ParseError, Config] = ???
  def readConfig(path:String): ZIO[Any, ConfigError, Config] =
    readFile(path).flatMap(parseConfig)
  override def run(args: List[String]): URIO[zio.ZEnv, ExitCode] = {
    val defaultConfig = Config
    val config = readConfig("config.json").orElseSucceed(defaultConfig)
    config.exitCode
  }

}


sealed trait ConfigError
final case class IOError(message: String) extends ConfigError
final case class ParseError(message: String) extends ConfigError

case class Config()