lazy val commonSettings = Seq(
  name         := "sparkstudy",
  organization := "pairwiseltd",
  version := "0.1.0-SNAPSHOT",
  scalaVersion := "2.11.11"  
)

val sparkVersion = "2.3.0"

lazy val root = (project in file("."))
  .settings(
    commonSettings,
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % "test",
    libraryDependencies ++= Seq(
			  "org.apache.spark" %% "spark-core" % sparkVersion,
      "org.apache.spark" %% "spark-sql" % sparkVersion
			)

  )
