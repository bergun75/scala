package com.pairwiseltd.study.spark.case1.ds

import com.pairwiseltd.study.spark.case1._
import com.pairwiseltd.study.spark.util.Timer
import org.apache.spark.sql.SparkSession

object SimpleDSSample extends AnyRef with Timer {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession
      .builder()
      .appName("Spark SQL basic example")
      .master(case1Master)
      .getOrCreate()

    import spark.implicits._
    val weatherReadingColNames = Seq(stationIdCol, xCol, monthCol, dayCol, temperatureCol)
    val weatherStationColNames = Seq(stationIdCol, yCol, latCol, lonCol)
    val dsWeatherReadingEntries =
      spark.read.csv(weather1975InputPath).toDF(weatherReadingColNames: _*)
        .selectExpr(stationIdCol,
          xCol,
          monthCol,
          dayCol,
          s"cast ($temperatureCol as double) $temperatureCol").as[WeatherReadingEntry]

    val dsStationEntries =
      spark.read.csv(stationInputPath).toDF(weatherStationColNames: _*)
      .selectExpr(stationIdCol,
        yCol,
        s"cast ($latCol as double) $latCol",
        s"cast ($lonCol as double) $lonCol"
        ).as[WeatherStation]
    val dsStationAvgReadings =
      dsWeatherReadingEntries.groupBy(stationIdCol).avg(temperatureCol)
        .withColumnRenamed(s"avg($temperatureCol)", "avgTemperature").as[AvgWeatherReadingEntry]
    val dsStationAvgReadingsWithStation = dsStationAvgReadings.as("A").join(dsStationEntries.as("B"),
      $"A.stationId" === $"B.stationId").drop($"B.stationId")
    val topAverageTemperatureDS = dsStationAvgReadingsWithStation.orderBy("avgTemperature").limit(100)
    topAverageTemperatureDS.explain(true)
    timed(topAverageTemperatureDS.write.format("parquet").save(weather1975AvgOutputPath))
  }
}

case class WeatherReadingEntry(stationId: String,
                               x: String,
                               month: String,
                               day: String,
                               temperature: Double)

case class AvgWeatherReadingEntry(stationId: String,
                                  avgTemperature: Double)

case class WeatherStation(stationId: String,
                          y: String,
                          lat: Double,
                          lon: Double)