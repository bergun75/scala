package com.pairwiseltd.study.spark.case1.rdd

import com.pairwiseltd.study.spark.util.Timer
import com.pairwiseltd.study.spark.case1._
import org.apache.spark.{SparkConf, SparkContext}

object SimpleRddSample extends AnyRef with Timer{
  def main(args: Array[String]) {
    val conf = new SparkConf().setAppName("SimpleSample").setMaster(case1Master)
    val sc = new SparkContext(conf)
    val textRdd = sc.textFile(weather1975InputPath)
    val csvRdd = textRdd.map(_.split(",").map(_.trim))
    val groupedByStation = csvRdd.groupBy(arr => arr(0)).
      mapValues(arrs => arrs.foldLeft(0.0){case (accTemp, arr) => accTemp + arr(4).toDouble} / arrs.size)
    groupedByStation.partitioner.foreach(println(_))
    timed(groupedByStation.top(100)(Ordering.by{case (_, temp) => -temp}).foreach{
      case (station, temp) => println(s"station=$station year 1975 averageTemp=$temp")
    })
  }
}
