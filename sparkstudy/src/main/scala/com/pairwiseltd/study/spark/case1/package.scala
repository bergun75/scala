package com.pairwiseltd.study.spark

import java.time.Instant

package object case1 {
  val stationIdCol = "stationId"
  val xCol = "x"
  val monthCol = "month"
  val dayCol = "day"
  val temperatureCol = "temperature"
  val avgTemperatureCol = "avgTemperature"

  val yCol = "y"
  val latCol = "lat"
  val lonCol = "lon"

  val weather1975InputPath = "src/main/resources/weather/1975.csv"
  val weather1975AvgOutputPath = s"target/parquet/weather/averages/${Instant.now.getEpochSecond}"
  val stationInputPath = "src/main/resources/stations/stations.csv"
  val case1Master = "local[*]"
}
