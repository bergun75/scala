package com.pairwiseltd.study.spark.util

trait Timer {
  def timed[T](block: => T): T = {
    val start = System.nanoTime
    val res = block
    val totalTime = (System.nanoTime - start) / 1000000L
    println("Elapsed time: %1d ms".format(totalTime))
    res
  }
}
