package com.pairwiseltd.study.spark.case1.df

import com.pairwiseltd.study.spark.util.Timer
import org.apache.spark.sql.SparkSession
import com.pairwiseltd.study.spark.case1._

object SimpleDFSample extends AnyRef with Timer {
  def main(args: Array[String]) {
    val spark = SparkSession
      .builder()
      .appName("Spark SQL Dataframe basic example")
      .master(case1Master)
      .getOrCreate()

    import spark.implicits._
    val newColNames = Seq(stationIdCol, xCol,monthCol, dayCol, temperatureCol)
    val df = spark.read.csv(weather1975InputPath)
    val stationDataDf = df.toDF(newColNames: _*)
    val typedStationDataDf = stationDataDf.selectExpr(stationIdCol,
      xCol,
      monthCol,
      dayCol,
      s"cast ($temperatureCol as double) $temperatureCol")
    val newColNames2 = Seq(stationIdCol, avgTemperatureCol)
    val stationGroupedDf = typedStationDataDf.groupBy(stationIdCol)
      .avg(temperatureCol).orderBy(s"avg($temperatureCol)").limit(100)
      .withColumnRenamed(s"avg($temperatureCol)",avgTemperatureCol)

    timed(stationGroupedDf.write.format("parquet").save(weather1975AvgOutputPath))

  }
}
