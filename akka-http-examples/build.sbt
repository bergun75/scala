lazy val commonSettings = Seq(
  name         := "akka-http-examples",
  organization := "pairwiseltd",
  version := "0.1.0-SNAPSHOT",
  scalaVersion := "2.11.8"
)

lazy val root = (project in file("."))
  .settings(
    commonSettings,
    libraryDependencies ++= {
    Seq(
        "com.typesafe.akka" %% "akka-http" % "10.0.6",
        "com.typesafe.akka" %% "akka-http-spray-json" % "10.0.6"
    )
  }
)
