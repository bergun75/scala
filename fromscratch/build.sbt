name := "fromscratch"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies += "junit" % "junit" % "4.10" % "test"
libraryDependencies ++= "org.apache.spark" %% "spark-core" % "2.1.0"