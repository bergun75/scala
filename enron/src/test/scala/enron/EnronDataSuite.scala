package enron

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.FunSuite
import scala.io.Source

@RunWith(classOf[JUnitRunner])
class EnronDataSuite extends FunSuite {

  trait TestData {
    val contentWithCC: String =
      Source.fromFile("src/test/resources/enron/3.869600.POWQBE2VCTANL0U1ZPD2Q2APMIXJ24ERA.txt").getLines.mkString("\n")
    val contentWithNothing: String =
      Source.fromFile("src/test/resources/enron/3.869396.JAA4TTKNJ0UJMM1W0PTPJD5R1Y4STIKNA.1.txt").getLines.mkString("\n")
    val contentWithoutCC: String =
      Source.fromFile("src/test/resources/enron/3.869259.PYEL0IQMXCXTHOIIL1BPMW1BHOQ3UUBMA.txt").getLines.mkString("\n")
    val contentWithMalId: String =
      Source.fromFile("src/test/resources/enron/3.869259.PYEL0IQMXCXTHOIIL1BPMW1BHOQ3UUBMB.txt").getLines.mkString("\n")
  }

  test("contentWithCC must be parsed correctly") {
    new TestData {
      assert(EnronData.parse(contentWithCC) ==
        Some(EnronEmail(1337402, EnronStats(3, 5, 357))), "must equal to EnronEmail(1337402, EnronStats(3, 5, 357))")

    }
  }

  test("contentWithNothing must be ignored correctly") {
    new TestData {
      assert(EnronData.parse(contentWithNothing) ==
        None, "must equal to None")

    }
  }

  test("contentWithMalId must be ignored correctly") {
    new TestData {
      assert(EnronData.parse(contentWithMalId) ==
        None, "must equal to None")

    }
  }

  test("contentWithoutCC must be parsed correctly") {
    new TestData {
      assert(EnronData.parse(contentWithoutCC) ==
        Some(EnronEmail(1174846, EnronStats(1, 0, 490))), "must equal to EnronEmail(1174846, EnronStats(1, 0, 490))")

    }

  }

}