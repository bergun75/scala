package enron

import scala.Ordering

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.storage.StorageLevel

import common.NEWLINE
import common.TAB
import common.timed

object EnronDataAnalysis {
  val HELP = "help"
  val TEST = "-t"
  val USAGE =
    NEWLINE +
      "Syntax" + NEWLINE +
      TAB + "run off_heap_size file_split_partitions enron_data_path [" + TEST + "]" + NEWLINE + NEWLINE +
      "Options" + NEWLINE + NEWLINE +
      TAB + "off_heap_size: Tungsten off heap memory size eg 1g" + NEWLINE + NEWLINE +
      TAB + "file_split_partitions: wholeTextFiles Spark Api minPartition argument. Tune this " +
      "according to your number of executors in your cluster as described in README file" + NEWLINE + NEWLINE +
      TAB + "enron_data_path: local, HDFS or S3 path to enron data" + NEWLINE + NEWLINE +
      TAB + "[" + TEST + "]: Option to indicate testing environment" + NEWLINE + NEWLINE +
      "Spark Environment Example" + NEWLINE + NEWLINE +
      TAB + "spark-submit --driver-memory 2g --executor-memory 4g --driver-cores 4 " +
      "--num-executors 16 /home/hadoop/enron_2.11-0.1.0-SNAPSHOT.jar 10g 64 s3n://enron-bucket/enron-data/**/*.txt" +
      NEWLINE + NEWLINE

  def main(args: Array[String]): Unit =
    {
      if (args.length == 1 && args(0).equalsIgnoreCase(HELP)) System.err.print(USAGE)
      else {
        require(args.length > 2, USAGE)
        val isTest = args.length == 4 && args(3).equalsIgnoreCase(TEST)
        val offHeapSize = args(0)
        val fileSplitPartitions = args(1).toInt
        val enronFilesDir = args(2)
        val conf = new SparkConf()
          .setAppName("Enron Analysis")
          .set("spark.memory.offHeap.size", offHeapSize)
          .set("spark.memory.offHeap.enabled", "true")
        // using builtin Tungsten columnar off-heap store and prevent GC pauses on persisted partitions 
        val sc = new SparkContext(
          if (isTest) conf.setMaster("local[*]").set("spark.executor.memory", "450g")
          else conf) // set executor and memory settings from spark-submit
        try {
          // TRANSFORMATIONS:
          val rddFileNameContent = sc.wholeTextFiles(enronFilesDir,
            fileSplitPartitions)

          val rddEnronEmails = rddFileNameContent.map {
            case (name, content) =>
              {
                val email = EnronData.parse(content)
                email match {
                  case Some(x) => (x.docId, x.stats)
                  case None    => (-1, EnronStats(0, 0, 0))
                }
              }
          }.filter(p => p._1 != -1)
            .persist(StorageLevel.OFF_HEAP)

          // ACTIONS:
          // It's action time!
          val (emailCount, wordCount) = timed {
            rddEnronEmails.aggregate((0L, 0L))(
              { case ((emailSum, wordSum), enronEmail) => (emailSum + 1, wordSum + enronEmail._2.wordCount) },
              (r, l) => (r._1 + l._1, r._2 + l._2))
          }
          val averageWordCount = wordCount / emailCount;
          println(s"averageWordCount = $averageWordCount")

          val first100 = timed {
            rddEnronEmails.top(100)(Ordering[Int].on { enronEmail =>
              (enronEmail._2.receipientToCount * 2 + enronEmail._2.recipientCcCount)
            })
          }
          first100.foreach(p => println(s"X-SDOC=${p._1} ${p._2}"))

        } finally {
          sc.stop()
        }
      }
    }
}
