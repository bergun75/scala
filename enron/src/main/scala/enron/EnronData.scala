package enron

import common.CC_TOKEN
import common.DOC_ID_TOKEN
import common.EML_ID_TOKEN
import common.EMPTY_STRING
import common.ENRON_EMAIL_SEPARATOR
import common.ENRON_INFO_TOKEN
import common.ENRON_STARS_TOKEN
import common.IDX_DOES_NOT_EXIST
import common.NEWLINE
import common.TO_TOKEN
import common.WHITESPACES
import common.WORD_SEPARATOR

case class EnronEmail(docId: Int, stats: EnronStats)
case class EnronStats(receipientToCount: Int, recipientCcCount: Int, wordCount: Int)

/** Factory for enron data parsing. */
object EnronData {
  /**
   * Parses content of a file provided as String
   *
   * @param content file content as String
   *
   * @return Some[EnronEmail] if parsed properly otherwise None.
   */
  private[enron] def parse(content: String): Option[EnronEmail] =
    {
      // NOTE avoiding NFA regex pattern matching because of non linear time gurantee!
      try {
        if (check(content)) {
          val toBeginIdx = findBgnIdx(content, TO_TOKEN)
          val docIdBeginIdx = findBgnIdx(content, DOC_ID_TOKEN, toBeginIdx)
          val docIdEndIdx = content.indexOf(NEWLINE, docIdBeginIdx)
          val ccBeginIdx = {
            val res = content.indexOf(CC_TOKEN, toBeginIdx)
            if (res == IDX_DOES_NOT_EXIST) IDX_DOES_NOT_EXIST
            else if (res > docIdBeginIdx) IDX_DOES_NOT_EXIST
            else res + CC_TOKEN.size
          }
          val (toEndIdx, ccEndIdx) =
            if (ccBeginIdx == IDX_DOES_NOT_EXIST) (docIdBeginIdx - 1, IDX_DOES_NOT_EXIST)
            else (ccBeginIdx - 1, docIdBeginIdx - 1)

          val messageBeginIdx = {
            val emlIdIdx = content.indexOf(EML_ID_TOKEN, docIdBeginIdx)
            content.indexOf(NEWLINE, emlIdIdx) + 1
          }
          val messageEndIdx =
            content.indexOf(ENRON_INFO_TOKEN, messageBeginIdx) - ENRON_STARS_TOKEN.size - 1
          if (toBeginIdx > docIdBeginIdx ||
            docIdBeginIdx > messageBeginIdx)
            None
          else
            Some(EnronEmail(
              content.substring(docIdBeginIdx, docIdEndIdx).replaceAll(WHITESPACES, EMPTY_STRING).toInt,
              EnronStats(
                content.substring(toBeginIdx, toEndIdx).split(ENRON_EMAIL_SEPARATOR).size,
                if (ccBeginIdx == -1) 0
                else content.substring(ccBeginIdx, ccEndIdx).split(ENRON_EMAIL_SEPARATOR).size,
                if (messageEndIdx > messageBeginIdx) content.substring(messageBeginIdx, messageEndIdx).split(WORD_SEPARATOR).size
                else 0)))
        } else None
      } catch {
        case er: Error    => throw er
        case e: Exception => None // TODO report malformed documents
      }
    }
  private def check(content: String): Boolean =
    // TODO report ignored documents
    if (!content.contains(DOC_ID_TOKEN)) false
    else if (!content.contains(TO_TOKEN)) false
    else if (!content.contains(ENRON_INFO_TOKEN)) false
    else true
  private def findBgnIdx(content: String, token: String): Int = content.indexOf(token) + token.size
  private def findBgnIdx(content: String, token: String, start: Int): Int = content.indexOf(token, start) + token.size
}
