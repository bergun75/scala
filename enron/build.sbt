lazy val commonSettings = Seq(
  name         := "enron",
  organization := "pairwiseltd",
  version := "0.1.0-SNAPSHOT",
  scalaVersion := "2.11.8",
  mainClass := Some("enron.EnronDataAnalysis")
)

lazy val root = (project in file("."))
  .settings(
    commonSettings,        
    libraryDependencies += "junit" % "junit" % "4.10" % "test",        
    libraryDependencies += "org.apache.spark" %% "spark-core" % "2.1.0",
    libraryDependencies += "org.apache.spark" %% "spark-streaming" % "2.1.0"
  )
