package funsets

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

/**
 * This class is a test suite for the methods in object FunSets. To run
 * the test suite, you can either:
 *  - run the "test" command in the SBT console
 *  - right-click the file in eclipse and chose "Run As" - "JUnit Test"
 */
@RunWith(classOf[JUnitRunner])
class FunSetSuite extends FunSuite {

  /**
   * Link to the scaladoc - very clear and detailed tutorial of FunSuite
   *
   * http://doc.scalatest.org/1.9.1/index.html#org.scalatest.FunSuite
   *
   * Operators
   *  - test
   *  - ignore
   *  - pending
   */

  /**
   * Tests are written using the "test" operator and the "assert" method.
   */
  // test("string take") {
  //   val message = "hello, world"
  //   assert(message.take(5) == "hello")
  // }

  /**
   * For ScalaTest tests, there exists a special equality operator "===" that
   * can be used inside "assert". If the assertion fails, the two values will
   * be printed in the error message. Otherwise, when using "==", the test
   * error message will only say "assertion failed", without showing the values.
   *
   * Try it out! Change the values so that the assertion fails, and look at the
   * error message.
   */
  // test("adding ints") {
  //   assert(1 + 2 === 3)
  // }

  import FunSets._

  test("contains is implemented") {
    assert(contains(x => x > 0, 100))
  }

  /**
   * When writing tests, one would often like to re-use certain values for multiple
   * tests. For instance, we would like to create an Int-set and have multiple test
   * about it.
   *
   * Instead of copy-pasting the code for creating the set into every test, we can
   * store it in the test class using a val:
   *
   *   val s1 = singletonSet(1)
   *
   * However, what happens if the method "singletonSet" has a bug and crashes? Then
   * the test methods are not even executed, because creating an instance of the
   * test class fails!
   *
   * Therefore, we put the shared values into a separate trait (traits are like
   * abstract classes), and create an instance inside each test method.
   *
   */

  trait TestSets {
    val s1 = singletonSet(1)
    val s2 = singletonSet(2)
    val s3 = singletonSet(3)
    val s4 = singletonSet(4)
  }

  /**
   * This test is currently disabled (by using "ignore") because the method
   * "singletonSet" is not yet implemented and the test would fail.
   *
   * Once you finish your implementation of "singletonSet", exchange the
   * function "ignore" by "test".
   */
  test("singletonSets contains right elements") {

    /**
     * We create a new instance of the "TestSets" trait, this gives us access
     * to the values "s1" to "s3".
     */
    new TestSets {
      /**
       * The string argument of "assert" is a message that is printed in case
       * the test fails. This helps identifying which assertion failed.
       */
      assert(contains(s1, 1), "Singleton")
      assert(contains(s2, 2), "Singleton")
      assert(contains(s3, 3), "Singleton")
    }
  }

  test("union contains all elements of each set") {
    new TestSets {
      val s = union(s1, s2)
      assert(contains(s, 1), "Union 1")
      assert(contains(s, 2), "Union 2")
      assert(!contains(s, 3), "Union 3")
    }
  }

  test("interscetion contains common elements of each set") {
    new TestSets {
      val s = union(s1, s2)
      val t = union(s1, s3)
      val z = intersect(s, t)
      assert(contains(z, 1), "Intersection 1")
      assert(!contains(z, 2), "Intersection 2")
    }
  }

  test("diff gives of difference of s1 to s2") {
    new TestSets {
      val s = union(s1, s2)
      val t = union(s1, s3)
      val z = diff(s, t)
      assert(contains(z, 2), "Diff 1")
      assert(!contains(z, 1), "Diff 2")
      assert(!contains(z, 3), "Diff 3")
    }
  }

  test("filter returns the subset of a set based on a predicates") {
    new TestSets {
      val s = union(s1, s2)
      val t = union(s3, s4)
      val z = union(s, t)
      val x = filter(z, x => x > 2)
      assert(!contains(x, 1), "Filter 1")
      assert(!contains(x, 2), "Filter 2")
      assert(contains(x, 3), "Filter 3")
      assert(contains(x, 4), "Filter 4")
    }
  }

  test("forall Returns whether all bounded integers within `s` satisfy `p`") {
    new TestSets {
      def sampleSet(): Set = x => (x < 100 && x > 0 && x % 4 == 0)
      val sample = sampleSet()
      assert(forall(sample, y => y % 2 == 0), "forall 1")
      assert(forall(sample, y => y % 4 == 0), "forall 2")
      assert(forall(sample, y => y < 200 && y > -200), "forall 3")
      assert(!forall(sample, y => y % 8 == 0), "forall 4")
      assert(!forall(sample, y => y % 3 == 0), "forall 5")
    }
  }

  test("exists whether there exists a bounded integer within `s` that satisfies `p`.") {
    new TestSets {
      def sampleSet(): Set = x => (x < 100 && x > 0 && x % 4 == 0)
      val sample = sampleSet()
      assert(exists(sample, y => y % 2 == 0), "exists 1")
      assert(exists(sample, y => y % 4 == 0), "exists 2")
      assert(!exists(sample, y => y < -100 && y > -200), "exists 3")
      assert(exists(sample, y => y % 8 == 0), "exists 4")
      assert(!exists(sample, y => y % 31 == 0), "exists 5")
    }
  }

  test("map Returns a set transformed by applying `f` to each element of `s`.") {
    new TestSets {
      def sampleSet(): Set = x => (x < 10 && x > 0 && x % 2 == 0)
      val sample = sampleSet()
      assert(exists(sample, y => y == 2), "exists 1")
      assert(exists(sample, y => y == 4), "exists 2")
      assert(exists(sample, y => y == 6), "exists 3")
      assert(exists(sample, y => y == 8), "exists 4")
      val newSet = map(sample, x => x * x)
      assert(exists(newSet, y => y == 4), "exists 1 after map")
      assert(exists(newSet, y => y == 16), "exists 2 after map")
      assert(exists(newSet, y => y == 36), "exists 3 after map")
      assert(exists(newSet, y => y == 64), "exists 4 after map")
    }
  }
}
