import org.apache.spark.{ SparkConf, SparkContext }
import org.apache.spark.sql.{ SparkSession, DataFrame }
import java.io.File

object SparkExcelExample {
  def main(args: Array[String]) {
    val conf = new SparkConf().setAppName("Excel to DataFrame").setMaster("local[*]")
    val sc = new SparkContext(conf)
    sc.setLogLevel("WARN")

    val spark = SparkSession.builder().getOrCreate()

    // Function to read xlsx file using spark-excel. 
    // This code format with "trailing dots" can be sent to IJ Scala Console as a block.
    def readExcel(file: String): DataFrame = spark.read.
      format("com.crealytics.spark.excel").
      option("location", file).
      option("useHeader", "true").
      option("sheetName", "Sheet2").
      option("treatEmptyValuesAsNulls", "true").
      option("inferSchema", "true").
      option("addColorColumns", "False").
      load()

    val dir = new File("./data/excel/")
    val excelFiles = dir.listFiles.sorted.map(f => f.toString) // Array[String]
    excelFiles.foreach(println(_))

    val dfs = excelFiles.map(f => readExcel(f)) // Array[DataFrame]
    val ppdf = dfs.reduce(_.union(_)) // DataFrame 

    ppdf.count() 
    ppdf.show(6)
  }
}