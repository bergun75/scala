lazy val commonSettings = Seq(
  name         := "test-spark-excel",
  organization := "pairwiseltd",
  version := "0.1.0-SNAPSHOT",
  scalaVersion := "2.11.8"
)

lazy val root = (project in file("."))
  .settings(
    commonSettings,
    libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.1",
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % "test",
    libraryDependencies += "org.scalaz" %% "scalaz-core" % "7.2.13",
		libraryDependencies += "org.apache.spark" %% "spark-core" % "2.1.0",
		libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.1.0",
		libraryDependencies += "com.crealytics" %% "spark-excel" % "0.8.4"

  )
