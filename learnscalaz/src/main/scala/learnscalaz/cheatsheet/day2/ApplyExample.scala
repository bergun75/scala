package learnscalaz.cheatsheet.day2

object ApplyExample extends App{
  /*
   * You can think of <*> (Apply) as a sort of a beefed-up fmap. Whereas 
   * map takes a function and a functor and applies the function inside 
   * the functor value, <*> (Apply) takes a functor that has a function 
   * in it and another functor and extracts that function from the first functor 
   * and then maps it over the second one.
   * 
   * trait Apply[F[_]] extends Functor[F] { self =>
        def ap[A,B](fa: => F[A])(f: => F[A => B]): F[B]
      }  
   */
  
   import scalaz._
   import Scalaz._
   
    val s = 9.some <*> {(_: Int) + 3}.some
    s.println
    
    // with some other notation
    val x = ^(3.some, 5.some) {_ + _}
    x.println
    
    // yet another style
    val y = (3.some |@| 5.some) {_ + _}
    y.println
    
    // Lists (actually the list type constructor, []) are applicative functors. What a surprise!
    // Lists with Apply
    val list1 = List(1, 2, 3) <*> List((_: Int) * 0, (_: Int) + 100, (x: Int) => x * x)
    list1.println
    
    val list2 = List(3, 4) <*> { List(1, 2) <*> List({(_: Int) + (_: Int)}.curried, {(_: Int) * (_: Int)}.curried) }
    list2.println
    
    val list3 = (List("ha", "heh", "hmm") |@| List("?", "!", ".")) {_ + _}   
    list3.println
    
    /*
     * However, [(+3),(*2)] <*> [1,2] could also work in such a way that the first function 
     * in the left list gets applied to the first value in the right one, the second function
     * gets applied to the second value, and so on. That would result in a list with two values, 
     * namely [4,4]. You could look at it as [1 + 3, 2 * 2].
     */
    
    val str1 = streamZipApplicative.ap (Tags.Zip(Stream(1, 2))) (Tags.Zip(Stream({(_: Int) + 3}, {(_: Int) * 2})))
    str1.asInstanceOf[Stream[Int]].toList.println
    
}