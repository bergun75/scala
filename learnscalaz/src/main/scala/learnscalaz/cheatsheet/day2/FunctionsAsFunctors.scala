package learnscalaz.cheatsheet.day2

object FunctionsAsFunctors extends App {
  import scalaz._
  import Scalaz._
  //  a functor is simply something that can be mapped over.
  val func1 = ((x: Int) => x + 1) map { _ * 7 }
  func1(9).println
  val func2 = Functor[List].lift { (_: Int) * 2 }
  func2(List(2, 3)).println
  val list1 = List(1, 2, 3) >| "x"
  list1.println
  val list2 = List(1, 2, 3) as "y"
  list2.println
  val list3 = List(1, 2, 3).fpair
  list3.println
  val list4 = List(1, 2, 3).strengthL("x")
  list4.println
  val list5 = List(1, 2, 3).strengthR("x")
  list5.println
  val list6 = List(1, 2, 3).void
  list6.println
}