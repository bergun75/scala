package learnscalaz.cheatsheet.day2

object ApplicativePointExamples extends App{
  // point is aliased to pure
  /*
   * pure should take a value of any type and return an applicative value with that value inside it. 
   * … A better way of thinking about pure would be to say that it takes a value and puts it in 
   * some sort of default (or pure) context—a minimal context that still yields that value.   * 
   */
  import scalaz._
  import Scalaz._
  
  val list1 = 1.point[List]
  list1.println
  // or use alias pure Scalaz prefers point
  val opt1 = 1.pure[Option]
  opt1.println
  
  val list2 = list1 map (_ + 2)
  list2.println
  
  val opt2 = opt1 map (_ + 2)
  opt2.println
  
  
}