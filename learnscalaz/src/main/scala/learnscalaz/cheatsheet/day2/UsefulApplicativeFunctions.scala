package learnscalaz.cheatsheet.day2

object UsefulApplicativeFunctions {

  def main(args: Array[String]) {
    import scalaz._
    import Scalaz._
  
    val func = Apply[Option].lift2((_: Int) :: (_: List[Int]))
    func(3.some, List(4).some).println

      /*
     * Let’s try implementing a function that takes a list of applicatives and returns 
     * an applicative that has a list as its result value. We’ll call it sequenceA.
     */

    
    def sequenceA[F[_]: Applicative, A](list: List[F[A]]): F[List[A]] = list match {
      case Nil     => (Nil: List[A]).point[F]
      case x :: xs => (x |@| sequenceA(xs)) { _ :: _ }
    }
    
    sequenceA(List(1.some, 2.some)).println
    sequenceA(List(3.some, none, 1.some)).println
    sequenceA(List(List(1, 2, 3), List(4, 5, 6))).println

  }

}