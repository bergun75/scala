package learnscalaz.cheatsheet.day1

// Scalaz equivalent for the Show typeclass is Show:
object ShowExamples {
  def main(args: Array[String]){
     import scalaz._
     import Scalaz._
     
     3.show.println     
     // Cord apparently is a purely functional data structure for potentially long Strings.
     3.shows.println
     3.println
     
  }  
}