package learnscalaz.cheatsheet.day1

object OrderExamples {
  def main(args: Array[String]){
    import scalaz._
    import Scalaz._
    
    // returns the ordering
    println( 1 ?|? 2)
    println( 2 ?|? 2)
    println( 3 ?|? 2)
    
    // 1 gt 2.0 // would not compile
    println (1.0 max 2.0)
    /*
     * Order enables ?|? syntax which returns Ordering: LT, GT, and EQ. 
     * It also enables lt, gt, lte, gte, min, and max operators by declaring order method. 
     * Similar to Equal, comparing Int and Double fails compilation.
     */
  }
}