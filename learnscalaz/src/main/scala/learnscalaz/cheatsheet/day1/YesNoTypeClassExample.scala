package learnscalaz.cheatsheet.day1

trait CanTruthy[A] { self =>
  /** @return true, if `a` is truthy. */
  def truthys(a: A): Boolean
}
object CanTruthy {
  def apply[A](implicit ev: CanTruthy[A]): CanTruthy[A] = ev
  def truthys[A](f: A => Boolean): CanTruthy[A] = new CanTruthy[A] {
    def truthys(a: A): Boolean = f(a)
  }
}
trait CanTruthyOps[A] {
  def self: A
  implicit def F: CanTruthy[A]
  final def truthy: Boolean = F.truthys(self)
}
object ToCanIsTruthyOps {
  implicit def toCanIsTruthyOps[A](v: A)(implicit ev: CanTruthy[A]) =
    new CanTruthyOps[A] {
      def self = v
      implicit def F: CanTruthy[A] = ev
    }
}

object YesNoTypeClassExample {
  def main(args: Array[String]) {
    
    implicit val intCanTruthy: CanTruthy[Int] = CanTruthy.truthys({
         case 0 => false
         case _ => true
       })
    import ToCanIsTruthyOps._
    println(10.truthy)
    println(0.truthy)
    
    implicit def listCanTruthy[A]: CanTruthy[List[A]] = CanTruthy.truthys({         
         case _   => true  
       })
    implicit val nilCanTruthy: CanTruthy[scala.collection.immutable.Nil.type] = CanTruthy.truthys(_ => false)   
    println(List("foo").truthy)
    println(Nil.truthy)
    
    implicit val booleanCanTruthy: CanTruthy[Boolean] = CanTruthy.truthys(identity)
    println(false.truthy)
    println(true.truthy)
    
    
    println(truthyIf (Nil) {"YEAH!"} {"NO!"})
  }
  def truthyIf[A: CanTruthy, B, C](cond: A)(ifyes: => B)(ifno: => C) = {
      import ToCanIsTruthyOps._
      if (cond.truthy) ifyes
      else ifno  
  }  
}

