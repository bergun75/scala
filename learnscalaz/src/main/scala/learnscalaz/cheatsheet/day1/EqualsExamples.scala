package learnscalaz.cheatsheet.day1

object EqualsExamples {
  def main(args: Array[String]){
    import scalaz._
    import Scalaz._
        
    println(1 === 2)
    println(1 == "asda") // would give warning but compile
    // println(1 === "someting") would give compile time error
    println(1 =/= 1 || false) // handy operator for winning against operator precedence
    println(1 == 1 || false)
    1 assert_=== 2 // nicer error str with RuntimeException
    assert (1 === 2)
    
  }
} 