package learnscalaz.cheatsheet.day1

object EnumExamples {
  def main(args: Array[String]){
    import scalaz._
    import Scalaz._
    
    val scalaStyle = 'a' to 'e' 
    val scalazStyle1 = 'a' |-> 'e' // List[Char]
    val scalazStyle2 = 'a' |=> 'e' // EphemeralStream[Char]
    
     'B'.succ.println
  }
}