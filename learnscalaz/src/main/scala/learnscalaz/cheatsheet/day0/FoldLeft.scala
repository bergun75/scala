package learnscalaz.cheatsheet.day0

trait FoldLeft[F[_]] {
  def foldLeft[A, B](xs: F[A], b: B, f: (B, A) => B): B
}
object FoldLeft {
  implicit val FoldLeftList: FoldLeft[List] = new FoldLeft[List] {
    def foldLeft[A, B](xs: List[A], b: B, f: (B, A) => B) = xs.foldLeft(b)(f)
  }
  implicit val FoldLeftSet: FoldLeft[Set] = new FoldLeft[Set] {
    def foldLeft[A, B](xs: Set[A], b: B, f: (B, A) => B) = xs.foldLeft(b)(f)
  }
}
