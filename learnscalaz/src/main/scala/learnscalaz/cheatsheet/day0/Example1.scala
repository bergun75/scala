package learnscalaz.cheatsheet.day0

object Example1 {
  def main(args: Array[String]) {
    println(sum(List("a", "b", "c")))
    println(sum(List(1, 2, 3)))
    
    
    println(sum2(Set("a", "b", "a")))
    println(sum2(Set(1, 2, 3, 3)))
    
    
  }
  def sum[A: Monoid](xs: List[A]): A = {
    val m = implicitly[Monoid[A]]
    xs.foldLeft(m.mzero)(m.mappend)
  }
  def sum2[M[_]: FoldLeft, A: Monoid](xs: M[A]): A = {
    val m = implicitly[Monoid[A]]
    val fl = implicitly[FoldLeft[M]]
    fl.foldLeft(xs, m.mzero, m.mappend)
  }

}


