lazy val commonSettings = Seq(
  name         := "learnscalaz",
  organization := "pairwiseltd",
  version := "0.1.0-SNAPSHOT",
  scalaVersion := "2.11.8"  
)

val scalazVersion = "7.1.0"

lazy val root = (project in file("."))
  .settings(
    commonSettings,
    libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.1",
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % "test",
    libraryDependencies ++= Seq(
			  "org.scalaz" %% "scalaz-core" % scalazVersion,
			  "org.scalaz" %% "scalaz-effect" % scalazVersion,
			  "org.scalaz" %% "scalaz-typelevel" % scalazVersion,
			  "org.scalaz" %% "scalaz-scalacheck-binding" % scalazVersion % "test"
			)

  )
