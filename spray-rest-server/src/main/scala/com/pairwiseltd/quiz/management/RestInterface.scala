package com.pairwiseltd.quiz.management

import scala.Vector
import scala.concurrent.duration.DurationInt

import org.json4s._
import org.json4s.native.JsonMethods._
import org.json4s.native.Serialization.read
import org.json4s.native.Serialization.write

import akka.actor.Actor
import akka.actor.ActorLogging
import akka.actor.PoisonPill
import akka.actor.Props
import akka.actor.actorRef2Scala
import akka.util.Timeout
import spray.http.StatusCodes
import spray.httpx.Json4sSupport
import spray.httpx.SprayJsonSupport.sprayJsonUnmarshaller
import spray.routing.Directive.pimpApply
import spray.routing.HttpService
import spray.routing.HttpServiceActor
import spray.routing.RequestContext
import spray.routing.Route

class RestInterface extends HttpServiceActor with RestApi {
  def receive = runRoute(routes)
}

object Json4sProtocol extends Json4sSupport {
  implicit def json4sFormats: Formats = DefaultFormats
}

trait RestApi extends HttpService with ActorLogging {
  actor: Actor =>
  implicit val timeout = Timeout(10 seconds)

  var quizzes = Vector[Quiz]()
  def routes: Route = pathPrefix("quizzes") {
    pathEnd {
      post {
        entity(as[Quiz]) { quiz =>
          requestContext: RequestContext =>
            val responder = createResponder(requestContext)
            createQuiz(quiz) match {
              case true => responder ! QuizCreated
              case _    => responder ! QuizAlreadyExists
            }
        }
      } ~
        get {
          requestContext: RequestContext =>
            val responder = createResponder(requestContext)
            responder ! QuizRetreived(getAllQuizes)
        }

    } ~
      path(Segment) { id =>
        delete {
          requestContext =>
            val responder = createResponder(requestContext)
            deleteQuiz(id)
            responder ! QuizDeleted
        }

      }
  }
  private def createQuiz(quiz: Quiz): Boolean = {
    val doesNotExist = !quizzes.exists(_.id == quiz.id)
    if (doesNotExist) quizzes = quizzes :+ quiz
    doesNotExist
  }

  private def getAllQuizes: Vector[Quiz] = quizzes

  private def deleteQuiz(id: String): Unit = {
    quizzes = quizzes.filterNot { _.id == id }
  }

  private def createResponder(requestContext: RequestContext) = {
    context.actorOf(Props(new Responder(requestContext)))
  }
}

class Responder(requestContext: RequestContext) extends Actor with ActorLogging {
  def receive = {
    case QuizCreated =>
      requestContext.complete(StatusCodes.Created)
      killYourself
    case QuizDeleted =>
      requestContext.complete(StatusCodes.OK)
      killYourself
    case QuizRetreived(x) =>
      import Json4sProtocol._
      requestContext.complete(x)
    case QuizAlreadyExists =>
      requestContext.complete(StatusCodes.Conflict)
      killYourself
  }
  private def killYourself = self ! PoisonPill
}