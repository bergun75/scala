package com.pairwiseltd.quiz.management

import com.typesafe.config.ConfigFactory

import akka.actor._
import akka.pattern.ask
import akka.actor.ActorSystem
import akka.actor.Props
import akka.util.Timeout

import scala.concurrent.duration._
import spray.can.Http
import akka.io.IO

object Main {
  def main(args: Array[String]): Unit = {
    val config = ConfigFactory.load("src/main/resources/rest.config")
    
    val host = "localhost"
    val port = 8080

    implicit val system = ActorSystem("quiz-management-service")

    val api = system.actorOf(Props(new RestInterface()), "httpInterface")

    implicit val executionContext = system.dispatcher
    implicit val timeout = Timeout(10 seconds)

    val httpManager = IO(Http)
    

    IO(Http).ask(Http.Bind(listener = api, interface = host, port = port))
      .mapTo[Http.Event]
      .map {
        case Http.Bound(address) =>
          println(s"REST interface bound to $address")
        case Http.CommandFailed(cmd) =>
          println("REST interface could not bind to " +
            s"$host:$port, ${cmd.failureMessage}")
          system.shutdown()
      }

  }
}