package com.pairwiseltd.quiz.management

import spray.json.DefaultJsonProtocol


object Quiz extends DefaultJsonProtocol {
    implicit val format = jsonFormat3(Quiz.apply)    
}
// entity: quiz
case class Quiz(id: String, question: String, correctAnswer: String)
 
// message: quiz has been created
case object QuizCreated
 
// message: quiz cannot be created because it already exists
case object QuizAlreadyExists
 
// message: quiz has been deleted
case object QuizDeleted

/// message: quiz has been retreived
case class QuizRetreived(quizes: Vector[Quiz])