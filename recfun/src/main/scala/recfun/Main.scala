package recfun
import scala.annotation.tailrec
import util.control.Breaks._

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
   * Exercise 1
   */
  def pascal(c: Int, r: Int): Int = {
    def pascalInternal(c: Int, r: Int): Int = {
      if (c < 0 || c > r) { 0 }
      else if (c == 0 || c == r) { 1 }
      else { pascalInternal(c - 1, r - 1) + pascalInternal(c, r - 1) }
    }
    pascalInternal(c, r)
  }

  /**
   * Exercise 2
   */
  def balance(chars: List[Char]): Boolean = {
    @tailrec
    def balanceInternal(chars: => List[Char], index: Int, counter: Int): Boolean = {
      if (index == chars.length) {
        counter == 0
      } else {
        if (counter < 0) {
          false
        } else {
          if (chars(index) == '(') {
            balanceInternal(chars, index + 1, counter + 1)
          } else if (chars(index) == ')') {
            balanceInternal(chars, index + 1, counter - 1)
          } else {
            balanceInternal(chars, index + 1, counter)
          }
        }
      }
    }
    if (chars.isEmpty) true else balanceInternal(chars, 0, 0)
  }

  /**
   * Exercise 3
   */
  def countChange(money: Int, coins: List[Int]): Int = {
    def countChangeInternal(money: Int, coins: => List[Int]): Int = {
      if (money == 0) {
        1
      } else if (money > 0 && !coins.isEmpty) {
        countChangeInternal(money - coins.head, coins) + countChangeInternal(money, coins.tail)
      } else {
        0
      }
    }
    def isOdd(number: Int) = { if ((number & 1) == 0) { false } else { true } }
    def anyOddCoin() = {
      for (i <- 0 to coins.length - 1) {
        if (isOdd(coins(i))) {
          true
        }
      }
      false
    }
    def validateInput(): Boolean = {
      if (money <= 0 || coins.length == 0) { false }
      else {
        if (isOdd(money)) {
          !anyOddCoin()
        } else { true }
      }
    }
    if (validateInput()) {
      countChangeInternal(money, coins)
    } else { 0 }
  }
}
