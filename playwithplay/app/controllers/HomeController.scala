package controllers

import javax.inject._
import play.api._
import play.api.mvc._

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class HomeController @Inject()(val controllerComponents: ControllerComponents) extends BaseController {

  /**
   * Create an Action to render an HTML page.
   *
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */
  def index() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def hello(name: String) = Action {
    val (u, l) = name.foldLeft((0,0)){case ((upper, lower), c) =>
      (upper + (if(c.isUpper) 1 else 0), lower + (if(c.isLower) 1 else 0))}
    Ok(views.html.hello(s"$name has $u upper and $l lower case chars."))
  }
}
