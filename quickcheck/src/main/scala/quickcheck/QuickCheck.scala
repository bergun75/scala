package quickcheck

import common._

import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._
import scala.annotation.tailrec

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  lazy val genHeap: Gen[H] = for {
    a <- arbitrary[A]
    b <- arbitrary[A]
    h <- oneOf(const(empty), genHeap)
  } yield insert(b, insert(a, h))

  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)

  property("findMin") = forAll { (h: H) =>
    val m = if (isEmpty(h)) 0 else findMin(h)
    findMin(insert(m, h)) == m
  }

  property("min1") = forAll { a: Int =>
    val h = insert(a, empty)
    findMin(h) == a
  }

  property("anyTwoElements") = forAll { (a: Int, b: Int) =>
    val min = a min b
    val max = a max b
    val h = insert(b, insert(a, empty))
    val h2 = deleteMin(h)
    val h3 = deleteMin(h2)
    isEmpty(h3) && findMin(h) == min && findMin(h2) == max
  }

  property("insertemptydeletemin") = forAll { a: Int =>
    val h = insert(a, empty)
    val h2 = deleteMin(h)
    isEmpty(h2)
  }

  property("findMinOfMeld") = forAll { (h: H, h2: H) =>
    val m = findMin(h)
    val m2 = findMin(h2)
    val mMin = m min m2
    val hRes = meld(h, h2)
    findMin(hRes) == mMin
  }

  property("deleteMinOnDistinctIntegersHeap") = forAll { (a: Int) =>
    val h = insert(6, insert(5, insert(4, insert(3, insert(2, insert(1, empty))))))
    val b = findMin(h)
    val h2 = deleteMin(h)
    val c = findMin(h2)
    b != c
  }

  property("deleteMinRecGivesSortedSequence") = forAll { (h: H) =>
    val m = findMin(h)
    isSorted(deleteMin(h), m)
  }

  @tailrec
  private def isSorted(hIn: H, prevMin: Int): Boolean = {
    if (isEmpty(hIn)) true
    else {
      val min = findMin(hIn)
      if (prevMin > min) false
      else isSorted(deleteMin(hIn), min)

    }
  }

}
