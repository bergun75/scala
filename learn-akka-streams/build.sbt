name := "learn-akka-streams"

version := "0.1"

scalaVersion := "2.12.8"

val AkkaVersion = "2.6.8"
libraryDependencies += "com.typesafe.akka" %% "akka-stream" % AkkaVersion