import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, OverflowStrategy}
import akka.stream.scaladsl.{Flow, Keep, Sink, Source}

import scala.util.{Failure, Success}


object AkkaStreamsRecap {
  def main(args: Array[String]): Unit = {
    implicit val system = ActorSystem("StreamRecap")
    implicit val materializer = ActorMaterializer

    val source = Source(1 to 100)
    val sink = Sink.foreach[Int](println)
    val flow = Flow[Int].map(x => x + 1)

    val runnableGraph = source.via(flow).to(sink)
    //val simpleMaterializedValue = runnableGraph.run() // Materialization

    // Materialized value
    val sumSink = Sink.fold[Int, Int](0) { (currentSum, elem) => currentSum + elem }
    /*val someFuture = source.runWith(sumSink)

      import system.dispatcher
      someFuture.onComplete{
        case Success(value) => println(s"sum=$value")
        case Failure(exception) => println(s"failed with $exception")
    }*/

    //val anotherMaterializedValue = source.viaMat(flow)(Keep.right).toMat(sink)(Keep.left).run()
    /*
    1 - Materializing a graph means materializing all the components
    2-) Materialized value can be ANYTHING ALL
     */
    /*
    Backpressure.
    - buffer elements
    - apply a strategy in case the buffer overflows.
    - fail the entire stream
     */

    val bufferedFlow = Flow[Int].buffer(10, OverflowStrategy.backpressure)
    source.async.via(bufferedFlow).async.runForeach(e => {
      Thread.sleep(100)
      println(e)
    })
  }
}
