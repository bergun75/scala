import java.nio.file.Paths

import akka.actor.ActorSystem
import akka.stream._
import akka.stream.scaladsl.{FileIO, Source}
import akka.util.ByteString
import akka.{Done, NotUsed}

import scala.concurrent._


object MainExample {

  def main(args: Array[String]): Unit = {
    implicit val system = ActorSystem("QuickStart")
    val source: Source[Int, NotUsed] = Source(1 to 100)
    val factorials = source.scan(BigInt(1))((acc, next) => acc * next)
    val result: Future[IOResult] =
      factorials.map(num => ByteString(s"$num\n")).runWith(FileIO.toPath(Paths.get("factorials.txt")))
    implicit val ec = system.dispatcher
    result.onComplete(_ => system.terminate())
  }

}
