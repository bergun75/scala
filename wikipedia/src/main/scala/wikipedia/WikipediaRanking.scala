package wikipedia

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.catalyst.expressions.MapValues

case class WikipediaArticle(title: String, text: String) {
  /**
   * @return Whether the text of this article mentions `lang` or not
   * @param lang Language to look for (e.g. "Scala")
   */
  def mentionsLanguage(lang: String): Boolean = {
    text.indexOf(lang + " ") == 0 || text.indexOf(" " + lang + " ") > 0
  }
}

object WikipediaRanking {

  val langs = List(
    "JavaScript", "Java", "PHP", "Python", "C#", "C++", "Ruby", "CSS",
    "Objective-C", "Perl", "Scala", "Haskell", "MATLAB", "Clojure", "Groovy")

  val conf: SparkConf = new SparkConf().setMaster("local[*]").setAppName("Wikipedia Trends")
  val sc: SparkContext = new SparkContext(conf)
  // Hint: use a combination of `sc.textFile`, `WikipediaData.filePath` and `WikipediaData.parse`
  val wikiRdd: RDD[WikipediaArticle] =
    sc.textFile(WikipediaData.filePath) map (text => WikipediaData.parse(text)) persist

  /**
   * Returns the number of articles on which the language `lang` occurs.
   *  Hint1: consider using method `aggregate` on RDD[T].
   *  Hint2: consider using method `mentionsLanguage` on `WikipediaArticle`
   */
  def occurrencesOfLang(lang: String, rdd: RDD[WikipediaArticle]): Int =
    rdd.aggregate(0)((count, article) => count + (if (article.mentionsLanguage(lang)) 1 else 0), _ + _)

  /* (1) Use `occurrencesOfLang` to compute the ranking of the languages
   *     (`val langs`) by determining the number of Wikipedia articles that
   *     mention each language at least once. Don't forget to sort the
   *     languages by their occurrence, in decreasing order!
   *
   *   Note: this operation is long-running. It can potentially run for
   *   several seconds.
   */
  def rankLangs(langs: List[String], rdd: RDD[WikipediaArticle]): List[(String, Int)] =
    langs map (lang => (lang, occurrencesOfLang(lang, rdd))) sortBy (-_._2)

  /* Compute an inverted index of the set of articles, mapping each language
   * to the Wikipedia pages in which it occurs.
   */
  def makeIndex(langs: List[String], rdd: RDD[WikipediaArticle]): RDD[(String, Iterable[WikipediaArticle])] =
    (rdd.flatMap(article => ((langs filter (article.mentionsLanguage(_)) map (lang => (lang, article))))) groupByKey).persist

  /* (2) Compute the language ranking again, but now using the inverted index. Can you notice
   *     a performance improvement?
   *
   *   Note: this operation is long-running. It can potentially run for
   *   several seconds.
   */
  def rankLangsUsingIndex(index: RDD[(String, Iterable[WikipediaArticle])]): List[(String, Int)] = {
    val s = index.mapValues(articles => articles.size)
    s.collect().toList sortBy (-_._2)
  }

  /* (3) Use `reduceByKey` so that the computation of the index and the ranking are combined.
   *     Can you notice an improvement in performance compared to measuring *both* the computation of the index
   *     and the computation of the ranking? If so, can you think of a reason?
   *
   *   Note: this operation is long-running. It can potentially run for
   *   several seconds.
   */
  def rankLangsReduceByKey(langs: List[String], rdd: RDD[WikipediaArticle]): List[(String, Int)] = {
    val res = rdd flatMap (article => (langs map (lang => (lang, if (article.mentionsLanguage(lang)) 1 else 0)))) persist
    val res2 = (res reduceByKey ((x: Int, y: Int) => x + y)).persist
    res2.collect().sortBy(-_._2).toList
  }

  def makeIndexReduceByKey(langs: List[String], rdd: RDD[WikipediaArticle]): RDD[(String, Int)] = {
    val res = rdd flatMap (article => (langs map (lang => (lang, if (article.mentionsLanguage(lang)) 1 else 0))))
    (res reduceByKey ((x: Int, y: Int) => x + y)).persist
  }

  def rankLangsReduceByKeyIndex(index: RDD[(String, Int)]): List[(String, Int)] = {
    index.collect().sortBy(-_._2).toList
  }

  def main(args: Array[String]) {

    /* Languages ranked according to (1) */
    val langsRanked: List[(String, Int)] = timed("Part 1: naive ranking", rankLangs(langs, wikiRdd))

    val langsRankeda: List[(String, Int)] = timed("Part 1: naive ranking second time", rankLangs(langs, wikiRdd))

    /* An inverted index mapping languages to wikipedia pages on which they appear */
    lazy val index: RDD[(String, Iterable[WikipediaArticle])] = timed("Part 2a making index", makeIndex(langs, wikiRdd))

    /* Languages ranked according to (2), using the inverted index */
    val langsRanked2: List[(String, Int)] = timed("Part 2b: ranking using inverted index", rankLangsUsingIndex(index))

    val langsRanked2a: List[(String, Int)] = timed("Part 2b: ranking using inverted index second time", rankLangsUsingIndex(index))

    /* Languages ranked according to (3) */
    val langsRanked3: List[(String, Int)] = timed("Part 3: ranking using reduceByKey", rankLangsReduceByKey(langs, wikiRdd))

    val langsRanked3a: List[(String, Int)] = timed("Part 3: ranking using reduceByKey second time", rankLangsReduceByKey(langs, wikiRdd))

    /* Languages ranked according to (4) */
    lazy val index2: RDD[(String, Int)] = timed("Part 4a making index", makeIndexReduceByKey(langs, wikiRdd))

    val langsRanked4: List[(String, Int)] = timed("Part 4b: ranking using reduceByKeyIndexed", rankLangsReduceByKeyIndex(index2))

    val langsRanked4a: List[(String, Int)] = timed("Part 4b: ranking using reduceByKeyIndexed second time", rankLangsReduceByKeyIndex(index2))

    /* Output the speed of each ranking */
    println(timing)
    sc.stop()
  }

  val timing = new StringBuffer
  def timed[T](label: String, code: => T): T = {
    val start = System.currentTimeMillis()
    val result = code
    val stop = System.currentTimeMillis()
    timing.append(s"Processing $label took ${stop - start} ms.\n")
    result
  }
}
