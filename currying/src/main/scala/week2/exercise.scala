package week2
import scala.annotation.tailrec

object exercise {
  def product(f: Int => Int)(a: Int, b: Int): Int = {
    @tailrec
    def loop(f: Int => Int)(a: Int, res: Int): Int = {
      if (a > b) res
      else loop(f)(a + 1, res * f(a))
    }
    loop(f)(a, 1)
  }
  def fact(n: Int) = product(x => x)(1, n)

  def main(args: Array[String]) {
    println(product(x => x)(1, 5))
    println(product2(x => x)(1, 5))
    println(sum(x => x)(1, 5))
    println(fact(7))
  }

  def mapReduce(f: Int => Int, combine: (Int, Int) => Int, zero: Int)(a: Int, b: Int): Int = {
    @tailrec
    def loop(a: Int, res: Int): Int = {
      if (a > b) res
      else loop(a + 1, combine(f(a), res))
    }
    loop(a, zero)
  }

  def product2(f: Int => Int)(a: Int, b: Int) = mapReduce(f, (x, y) => x * y, 1)(a, b)
  def sum(f: Int => Int)(a: Int, b: Int) = mapReduce(f, (x, y) => x + y, 0)(a, b)
}
