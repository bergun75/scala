package week9

object StreamRangeTest extends App {
  def streamRange(lo: Int, hi: Int): Stream[Int] = {
    print(lo + " ")
    if (lo >= hi) Stream.empty
    else lo #:: streamRange(lo + 1, hi)
  }
  val stream1 = streamRange(1, 10)
  println(stream1)
  println(stream1.take(3).toList)
  // So powerful. Because the return type of the function is Stream
  // the returned object is the 1 #:: ? which is Stream (1, ?)
  // and the function on how to continue to create the range is also 
  // stored in the projection information and that projection is evaluated until 
  // an operation requires the rest of the elements until when needed.
}