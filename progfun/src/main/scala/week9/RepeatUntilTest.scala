package week9

import scala.annotation.tailrec

object RepeatUntilTest extends App {

  @tailrec
  def REPEAT(command: => Unit)(condition: => Boolean): Unit = {
    command
    if (condition) ()
    else REPEAT(command)(condition)
  }
  var x = 0;
  REPEAT { x += 1; println("v") }(x == 10)

  def REPEAT2(command: => Unit): COMMAND = {
    new COMMAND(command)
  }

  class COMMAND(command: => Unit) {
    def UNTIL(condition: => Boolean): Unit = {
      command
      if (condition) ()
      else UNTIL(condition)
    }
  }

  var x2 = 0;
  REPEAT2 { x2 += 1; println("x") } UNTIL (x2 == 10)

}