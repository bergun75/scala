package cheatsheet.files.basic

import scala.io.Source

object Examples {
  def main(args: Array[String]){
    Source.fromFile("src/main/resources/sample").foreach { x => print(x) }
    println()
    val res = Source.fromFile("src/main/resources/sample").foldLeft("")((acc, x) => acc + x)
    println(res)
  }
}