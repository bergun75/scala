package cheatsheet.function.parameters.named

object Examples {

  def main(args: Array[String]): Unit = {
    println(speed(time = 7, distance = 100))
    // Named arguments pay attention to the argument order!
    // It is not same as the func declaration
  }

  def speed(distance: Float, time: Float): Float = {
    require(time != 0.0F)
    distance / time
  }
}