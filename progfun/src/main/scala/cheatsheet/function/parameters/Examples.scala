package cheatsheet.function.parameters

object Examples{
  def main(args: Array[String]) {
    val arr = Array("Naber", "lan", "dingil", "?")    
    echo(arr: _*) // This is how every element of arr applied as separate sting args
  }
  def echo(args: String*) =
    for (arg <- args) println(arg)

}