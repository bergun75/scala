package cheatsheet.packageprotection

object Examples {
  
}

package navigation {
  private[packageprotection] class Navigator{
    protected[navigation] def useStartChart(){}
    class LegOfJourney {
      private[Navigator] val distance = 100
    }
    private[this] var speed = 200
    private val sample = new LegOfJourney
    println(sample.distance)
  }  
}

package launch {
  import navigation._
  object Vehicle{
    private[launch] val guide = new Navigator
  }
}