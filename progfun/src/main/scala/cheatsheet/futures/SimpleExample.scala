package cheatsheet.futures

import scala.util.{ Success, Failure }
import scala.concurrent.Future
import scala.concurrent.ExecutionContext

object SimpleExample {

  def main(args: Array[String]) {

    implicit val ec = ExecutionContext.global

    def f: Future[Int] = Future {
      java.util.concurrent.ThreadLocalRandom.current.nextInt
    }

    val fs = (0 until 10) map { _ => f }
    fs.foreach(f =>
      f onComplete {
        case Success(res) => println(res)
        case Failure(t)   => println("An error has occured " + t.getMessage)
      })
    

    Future.sequence(fs).onComplete(_ => println("Terminated"))

  }

}

