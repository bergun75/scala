package cheatsheet.futures

import scala.concurrent.Future
import java.util.concurrent.ThreadLocalRandom
import scala.concurrent.ExecutionContext
import scala.util.{Success, Failure}
import java.util.NoSuchElementException
import scala.util.control.NonFatal

object FuturesForExpressions {
  implicit val ec = ExecutionContext.global
  
  def main(args: Array[String]) {
    val pair = for{
      x <- nextInt
      y <- nextInt
      if(x > y)
    }yield (x, y)
    
    pair onComplete {
      case Success(res) => println(res)
      case Failure(t) => {
        t match {
          case t: NoSuchElementException => println()
          case _ => println("serious problem")
        }
      }
    }
    
    Future.sequence(Seq(pair)).onComplete(_ => println("finished"))
  }

  def nextInt: Future[Int] = {
    Future {
      ThreadLocalRandom.current.nextInt
    }
  }
}