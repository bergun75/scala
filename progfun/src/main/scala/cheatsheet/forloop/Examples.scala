package cheatsheet.forloop
/*
The treatment of for loops is similar to the For-Comprehensions commonly used in functional programming. 
The general expression for for loop equivalent in Scala is

for(v1 <- e1; v2 <- e2; ...; v_n <- e_n) command

Note a few subtle differences from a For-expreesion. 
There is no yield expression, command can contain mutable states and e1, e2, ..., e_n 
are expressions over arbitrary Scala collections. 
This for loop is translated by Scala using a foreach combinator defined over any arbitrary collection. 
The signature for foreach over collection T looks like this

def foreach(f: T => Unit) : Unit

Using foreach, the general for loop is recursively translated as follows:

for(v1 <- e1; v2 <- e2; ...; v_n <- e_n) command = 
    e1 foreach (v1 => for(v2 <- e2; ...; v_n <- e_n) command)
 */
object Examples extends App {
  val sample = List(1, 2, 3)

  for (v1 <- sample) println(v1)

  val sample2 = List('a', 'b', 'c')
  for (v1 <- sample; v2 <- sample2) println(s"v1=$v1 v2=$v2")  

}