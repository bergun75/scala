package cheatsheet.patternmatching

// Pattern matches are also used quite often in anonymous functions:

object AnonymousFunctions {
  val pairs: List[(Char, Int)] = ('a', 2) :: ('b', 3) :: Nil
  val chars: List[Char] = pairs.map(p => p match {
    case (ch, num) => ch
  })
  // Instead of p => p match { case ... }, you can simply write {case ...}, 
  // so the above example becomes more concise:
  
  val chars2: List[Char] = pairs map {
    case (ch, num) => ch
  }
}