package cheatsheet.patternmatching

/*
Pattern matching is used for decomposing data structures:

unknownObject match {
  case MyClass(n) => ...
  case MyClass2(a, b) => ...
}

Here are a few example patterns

(someList: List[T]) match {
  case Nil => ...          // empty list
  case x :: Nil => ...     // list with only one element
  case List(x) => ...      // same as above
  case x :: xs => ...      // a list with at least one element. x is bound to the head,
                           // xs to the tail. xs could be Nil or some other list.
  case 1 :: 2 :: cs => ... // lists that starts with 1 and then 2
  case (x, y) :: ps => ... // a list where the head element is a pair
  case _ => ...            // default case if none of the above matches
}

The last example shows that every pattern consists of sub-patterns: 
it only matches lists with at least one element, where that element is a pair. 
x and y are again patterns that could match only specific types.

*/

object Examples extends App {
  private final def processShapes(ys: List[Shape2D]): Unit = {
    ys match {
      case x :: xs =>
        printShape(x)
        processShapes(xs)
      case Nil => println("shape list processed")
    }
  }

  private final def printShape(shape: Shape2D) = {
    val shapeType: String = shape match {
      case Circle(_)       => "Circle"
      case Square(_)       => "Square"
      case Rectangle(_, _) => "Rectangle"
      case _               => "Unknown"
    }
    println(s"$shapeType area: ${shape.surfaceArea} perimeter: ${shape.perimeter}")
  }

  val circle = new Circle(1.0)
  val square = new Square(1.0)
  val rectangle = new Rectangle(1.0, 2.0)
  val listOfShapes = List[Shape2D](circle, square, rectangle)
  processShapes(listOfShapes)

}

trait Shape2D {
  def surfaceArea: Double
  def perimeter: Double
}

case class Circle(radius: Double) extends Shape2D {
  def surfaceArea: Double = Math.PI * Math.pow(this.radius, 2.0)
  def perimeter: Double = 2 * Math.PI * this.radius
}

case class Square(length: Double) extends Shape2D {
  def surfaceArea: Double = Math.pow(this.length, 2.0)
  def perimeter: Double = 4 * this.length
}

case class Rectangle(horizantalLength: Double, verticalLength: Double) extends Shape2D {
  def surfaceArea: Double = this.horizantalLength * this.verticalLength
  def perimeter: Double = this.horizantalLength * 2 + this.verticalLength * 2
}


