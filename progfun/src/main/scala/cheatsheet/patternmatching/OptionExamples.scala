package cheatsheet.patternmatching

/*
Pattern matching can also be used for Option values. 
Some functions (like Map.get) return a value of type Option[T] which is either a value of type 
Some[T] or the value None:
*/

object OptionExamples extends App {
  val myMap = Map("a" -> 42, "b" -> 43)
  def getMapValue(s: String): String = {
    myMap get s match {
      case Some(nb) => "Value found: " + nb
      case None     => "No value found"
    }
  }

  // Most of the times when you write a pattern match on an option value, 
  // the same expression can be written more concisely using combinator methods of the Option class. 
  // For example, the function getMapValue can be written as follows:
  def getMapValue2(s: String): String =
    myMap.get(s).map("Value found: " + _).getOrElse("No value found")

  println(getMapValue2("a")) // "Value found: 42"
  println(getMapValue2("c")) // "No value found"
}