package cheatsheet.patternmatching

object StringExamples extends App {
  val csvStrs = Seq("baris,ergun,42,engineer", "deniz,ergun,,trainer", "poyraz,ergun,7", "celal,ergun", "aysel")
  val csvStrPattern = "(\\w*),(\\w*),(\\d*),(\\w*)".r
  val csvStrPattern2 = "(\\w*),(\\w*),(\\d*)".r
  val csvStrPattern3 = "(\\w*),(\\w*)".r
  val csvStrPattern4 = "(\\w*)".r
  val csvStrsMapped = csvStrs.map { csvStr =>
    csvStr match {
      case csvStrPattern(name, surname, age, occup) => {
        (optionizeStr(name), optionizeStr(surname), optionizeStr(age), optionizeStr(occup))
      }
      case csvStrPattern2(name, surname, age) => {
        (optionizeStr(name), optionizeStr(surname), optionizeStr(age), None)
      }
      case csvStrPattern3(name, surname) => {
        (optionizeStr(name), optionizeStr(surname), None, None)
      }
      case csvStrPattern4(name) => {
        (optionizeStr(name), None, None, None)
      }
      case _                                        => (None, None, None, None)
    }  
  }
  
  println(csvStrsMapped)

	val strOption1 = Some("someStr")
	val strOption2 = None
  val str1 = "deneme"
	val replacedStr1 = str1 match {
		case "deneme" if strOption1.isDefined => strOption1.get
		case _ => ""
	}
	val replacedStr2 = str1 match {
		case "deneme" if strOption2.isDefined => strOption1.get
		case _ => ""
	}
	println(s"replacedStr1 = $replacedStr1")
	println(s"replacedStr2 = $replacedStr2")
  
  def optionizeStr(str: String): Option[String] = {
    if(str.isEmpty) None else Some(str)
  }

}
