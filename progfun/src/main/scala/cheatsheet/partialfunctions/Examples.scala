package cheatsheet.partialfunctions

/*

A subtype of trait Function1 that is well defined on a subset of its domain.

Every concrete implementation of PartialFunction has the usual apply method 
along with a boolean method isDefinedAt.

Important: An implementation of PartialFunction can return true for isDefinedAt 
but still end up throwing RuntimeException (like MatchError in pattern-matching implementation).

A concise way of constructing partial functions is shown in the following example:
*/

object Examples extends App {

  val pf: PartialFunction[Coin, String] = {
    case Gold() => "a golden coin"
    case x => s"no support for $x"
  }

  println(pf.isDefinedAt(Gold()))
  println(pf.isDefinedAt(Silver()))
  println(pf(Gold()))
  println(pf(Silver()))

}

sealed trait Coin {}
case class Gold() extends Coin {}
case class Silver() extends Coin {}