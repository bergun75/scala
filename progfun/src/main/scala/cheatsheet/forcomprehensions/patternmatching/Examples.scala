package cheatsheet.forcomprehensions.patternmatching

/*
One can also use Patterns inside for-expression. 
The simplest form of for-expression pattern looks like

	for { pat <- expr} yield e
where pat is a pattern containing a single variable x. 
We translate the pat <- expr part of the expression to

  x <- expr withFilter {
    case pat => true
    case _ => false
  } map {
    case pat => x
  }
  
The remaining parts are translated to map, flatMap, withFilter 
according to standard for-comprehension rules.

 */

object Examples extends App {
  val arguments = Array("h=2", "b=3" , "irrelevant")
  val property = """(.+)=(.+)""".r
  
  val results = for {
    property(key, value) <- arguments
  } yield (Symbol(key), value)
  
  val mapOfResults = Map(results:_*)
  println(mapOfResults)
  
}