package cheatsheet.forcomprehensions.randomgenerator

/*
The map and flatMap methods can be overridden to make a for-expression versatile, 
for example to generate random elements from an arbitrary collection like lists, 
sets etc. Define the following trait Generator to do this.

  trait Generator[+T] { self =>
    def generate: T
    def map[S](f: T => S) : Generator[S] = new Generator[S] {
      def generate = f(self.generate)
    }
    def flatMap[S](f: T => Generator[S]) : Generator[S] = new Generator[S] {
      def generate = f(self.generate).generate
    }
  }

*/

object Examples extends App {
  val integers = new Generator[Int] {
    val rand = java.util.concurrent.ThreadLocalRandom.current()
    def generate = rand.nextInt()
  }

  // With these definition, and a basic definition of integer 
  // generator, we can map it to other domains like booleans, 
  // pairs, intervals using for-expression magic

  val booleans = for { x <- integers } yield x > 0
  val pairs = for { x <- integers; y <- integers } yield (x, y)
  def interval(lo: Int, hi: Int): Generator[Int] =
    for { x <- integers } yield lo + x % (hi - lo)

  val generatedInts = (0 until 100) map (_ => integers.generate)
  val generatedBools = (0 until 100) map (_ => booleans.generate)
  val generatedPairs = (0 until 100) map (_ => pairs.generate)
  val generatedIntsInRange = (0 until 100) map (_ => interval(0, 100).generate)
  println(generatedInts)
  println(generatedBools)
  println(generatedPairs)
  println(generatedIntsInRange)

}

trait Generator[+T] { self =>
  def generate: T
  def map[S](f: T => S): Generator[S] = new Generator[S] {
    def generate = f(self.generate)
  }
  def flatMap[S](f: T => Generator[S]): Generator[S] = new Generator[S] {
    def generate = f(self.generate).generate
  }
}
