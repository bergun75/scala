package cheatsheet.variance

/*
Given A <: B
If C[A] <: C[B], C is COVARIANT
If C[A] >: C[B], C is CONTRAVARIANT
Otherwise C is NONVARIANT
 
class C[+A] { ... } // C is COVARIANT
class C[-A] { ... } // C is CONTRAVARIANT
class C[A]  { ... } // C is NONVARIANT

For a function, if A2 <: A1 and B1 <: B2, then A1 => B1 <: A2 => B2.

In Other words:
If A2 is a subtype (<:) of A1 and B1 is a subtype of (<:) B2 then
A1 => B1 function type is a subtype (<:) of A2 => B2 function type.

In Other Words

A1 => B1
^  v  v
A2 => B2

As a result:

Functions must be CONTRAVARIANT in their argument types and 
COVARIANT in their result types, e.g.
 
*/

object Examples extends App {
  
}

trait Function1[-T, +U] {
  def apply(x: T):U
}

trait Array[+T]{
  //def update (x : T) // variance check fails
}
