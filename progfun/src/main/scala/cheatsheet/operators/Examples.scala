package cheatsheet.operators

// myObject myMethod 1 is the same as calling myObject.myMethod(1)

// Operator (i.e. function) names can be alphanumeric, symbolic (e.g. x1, *, +?%&, vector_++, counter_=)

// The precedence of an operator is determined by its first character, 
// with the following increasing order of priority:

/*
(all letters)
|
^
&
< >
= !
:
+ -
* / %
(all other special characters)

The associativity of an operator is determined by its last character: 
Right-associative if ending with :, Left-associative otherwise.

Note that assignment operators have lowest precedence. 
(Read Scala Language Specification 2.9 sections 6.12.3, 6.12.4 for more info)

*/

object Examples extends App {

  // Precedence
  println(3 * 3 + 2 == 2 + 3 * 3) // must print true

  // Left associative operator + impl
  println(new MyClass(2) + new MyClass(3))
  val three = new MyClass(3)
  println(-three) // should print minus 3
  println(new MyClass(5) :: three)

}

class MyClass(x: Int) { // Defines a new type MyClass with a constructor  
  val linkedClasses = List[MyClass](this)
  def getX = x // public method computed every time it is calledprivate def test(a: Int): Int = { ??? } // private method
  def unary_- : MyClass = new MyClass(-x)
  def +(that: MyClass): MyClass = {
    new MyClass(this.getX + that.getX)
  }
  def ::(that: MyClass): List[MyClass] = {
    that :: linkedClasses
  }
  override def toString = // overridden method  
    x + ""
}