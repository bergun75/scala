package cheatsheet.monads.sideeffects

import scala.util.{ Try, Success, Failure }

/*
Monads and their operations like flatMap help us handle programs with 
side-effects (like exceptions) elegantly. This is best demonstrated by a Try-expression. 
Note: Try-expression is not strictly a Monad because it does not satisfy all 
three laws of Monad mentioned above. Although, it still helps handle expressions with exceptions.

The parametric Try class as defined in Scala.util looks like this:

abstract class Try[T]
case class Success[T](elem: T) extends Try[T]
case class Failure(t: Throwable) extends Try[Nothing]

Try[T] can either be Success[T] or Failure(t: Throwable)

*/

object Examples extends App {
  def answerToLife(nb: Int): Try[Int] = {
    if (nb == 42) Success(nb)
    else Failure(new Exception("WRONG"))
  }

  val result = answerToLife(42) match {
    case Success(t)           => t // returns 42
    case failure @ Failure(e) => failure // returns Failure(java.Lang.Exception: WRONG)
  }

  val result2 = answerToLife(41) match {
    case Success(t)           => t // returns 42
    case failure @ Failure(e) => failure // returns Failure(java.Lang.Exception: WRONG)
  }

  println(result)
  println(result2)

  // Now consider a sequence of scala method calls:
  val w1 = Examples
  val w2 = w1.answerToLife(41)
  // All of these method calls are synchronous, blocking and the sequence computes to completion as 
  // long as none of the intermediate methods throw an exception. But what if one of the methods, 
  // say f2 does throw an exception? The Try class defined above helps handle these exceptions elegantly, 
  // provided we change return types of all methods f1, f2, ... to Try[T]. 
  // Because then, the sequence of method calls translates into an elegant for-comprehension:

  val o1 = Examples
  val ans = for {
    o2 <- o1.answerToLife(41)
  } yield o2
  println(ans)

  // This transformation is possible because Try satisfies 2 properties related to flatMap and unit of a monad. 
  // If any of the intermediate methods f1, f2 throws and exception, value of ans becomes Failure. 
  // Otherwise, it becomes Success[T].
}