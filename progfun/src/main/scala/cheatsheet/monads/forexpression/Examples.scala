package cheatsheet.monads.forexpression

import scala.util.Random

/*
Monads help simplfy for expressions.
Associativity helps us "inline" nested for-expressions and write something like.

for { x <- e1; y <- e2(x) ... }

Right unit helps us eliminate for-expression using identity

for {x <- m} yield x == m
 */
object Examples extends App {

  val foo = new Foo
  val res =
    for {
      i <- (1 to 30)
      x <- foo.giveBar
      y <- x.giveBaz
      z <- y.compute
    } yield z
  println(res)
}

class Foo {
  def giveBar: Option[Bar] = {
    val shall = scala.util.Random.nextBoolean()
    if (shall) Some(new Bar) else None
  }
}

class Bar {
  def giveBaz: Option[Baz] = {
    val shall = scala.util.Random.nextBoolean()
    if (shall) Some(new Baz) else None
  }
}

class Baz {
  def compute: Option[Int] = {
    val shall = scala.util.Random.nextBoolean()
    if (shall) Some(scala.util.Random.nextInt) else None
  }
}
