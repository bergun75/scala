package cheatsheet.monads

import scala.util.Try
import scala.util.Failure
import scala.util.Success



/*
  A monad is a parametric type M[T] with two operations: flatMap and unit.
  
  trait M[T] {
    def flatMap[U](f: T => M[U]) : M[U]
    def unit[T](x: T) : M[T]
  }
  
  These operations must satisfy three important properties:

	Associativity: (x flatMap f) flatMap g == x flatMap (y => f(y) flatMap g)

	Left unit: unit(x) flatMap f == f(x)

	Right unit: m flatMap unit == m
	
	Many standard Scala Objects like List, Set, Option, Gen are monads with identical 
	implementation of flatMap and specialized implementation of unit. 
	An example of non-monad is a special Try object that fails with 
	a non-fatal exception because it fails to satisfy Left unit (See lectures).
	Try(expr) flatMap f != f(expr)
	because f(expr) might throw an exception but the left hand side cannot.
*/

object Examples extends App {

  // List is a Monad
  val sample = List(1, 2, 3)
  val f_List = (x: Int) => List[Int](x, 0)
  val g_List = (x: Int) => List[Int](x)
  val firstList = (sample flatMap f_List) flatMap g_List
  val secondList = sample flatMap (y => f_List(y) flatMap g_List)
  assert(firstList == secondList)
  val leftUnitLeftHand_List = g_List(3) flatMap f_List
  val leftUnitRightHand_List = f_List(3)
  assert(leftUnitLeftHand_List == leftUnitRightHand_List)
  val rightUnitLeftHand_List = sample flatMap g_List
  val rightUnitRightHand_List = sample
  assert(rightUnitLeftHand_List == rightUnitRightHand_List)

  // Set is a Monad  
  val f_Set = (x: Int) => Set[Int](x, 0)
  val g_Set = (x: Int) => Set[Int](x)
  val firstSet = (sample flatMap f_Set) flatMap g_Set
  val secondSet = sample flatMap (y => f_Set(y) flatMap g_Set)
  assert(firstSet == secondSet)
  val leftUnitLeftHand_Set = g_Set(3) flatMap f_Set
  val leftUnitRightHand_Set = f_Set(3)
  assert(leftUnitLeftHand_Set == leftUnitRightHand_Set)
  val rightUnitLeftHand_Set = sample flatMap g_Set
  val rightUnitRightHand_Set = sample
  assert(rightUnitLeftHand_Set == rightUnitRightHand_Set)

  // Option is a Monad
  val sampleOption = Option("asdasd")
  val f_Option = (x: String) => Option(x + "new")
  val g_Option = (x: String) => Option(x)
  val firstOption = (sampleOption flatMap f_Option) flatMap g_Option
  val secondOption = sampleOption flatMap (y => f_Option(y) flatMap g_Option)
  assert(firstOption == secondOption)
  val leftUnitLeftHand_Option = g_Option("deneme") flatMap f_Option
  val leftUnitRightHand_Option = f_Option("deneme")
  assert(leftUnitLeftHand_Option == leftUnitRightHand_Option)
  val rightUnitLeftHand_Option = sampleOption flatMap g_Option
  val rightUnitRightHand_Option = sampleOption
  assert(rightUnitLeftHand_Option == rightUnitRightHand_Option)
  
  def whatIsTheBestAge(x :Int): Try[Int] = 
    if(x > 40 && x < 43) Success(x)
    else Failure(new IllegalArgumentException("WRONG age 41-42 is the BEST"))
  
  // Try only follow 2 properties of a Monad
  val sampleTry = whatIsTheBestAge(41)
  val f_Try = (x: Int) => whatIsTheBestAge(x + 1)
  val g_Try = (x: Int) => whatIsTheBestAge(x)  
  val firstTry =  (sampleTry flatMap f_Try) flatMap g_Try
  val secondTry =  sampleTry flatMap (y => f_Try(y) flatMap g_Try)  
  assert(firstTry == secondTry)
  
  // Success(throw new Exception) flatMap f == f(throw new Exception) // holds
  // but
  // Success(s) flatMap (x => throw new Exception) == Failure(new Exception) // does not hold! 
  val f = (x: Any) => (throw new Exception)
  val leftUnitLeftHandTry = sampleTry flatMap f
  val leftUnitRightHandTry = Failure(new Exception)
  println (leftUnitLeftHandTry)
  println (leftUnitRightHandTry)
  assert(leftUnitLeftHandTry != leftUnitRightHandTry)
  
  val rightUnitLeftHand_Try = sampleTry flatMap g_Try
  val rightUnitRightHand_Try = sampleTry
  assert(rightUnitLeftHand_Try == rightUnitRightHand_Try)


}