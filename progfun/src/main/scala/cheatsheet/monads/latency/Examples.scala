package cheatsheet.monads.latency

import scala.concurrent._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.util.Failure
import scala.util.Success
import scala.util.Try


object Examples extends App {

  // The function to be run asynchronously
  val answerToLife: Future[Int] = Future {
    42
  }

  // These are various callback functions that can be defined  
  answerToLife onComplete {
    case Success(result) =>
      println(result)
      result
    case Failure(t) => println("An error has occured: " + t.getMessage)
  }

  answerToLife onSuccess {
    case result =>
      println(result)
      result
  }

  answerToLife onFailure {
    case t => println("An error has occured: " + t.getMessage)
  }
  /* 
Combinators on Future

A Future is a Monad and has map, filter, flatMap defined on it. 
In addition, Scala's Futures define two additional methods:

  def recover(f: PartialFunction[Throwable, T]): Future[T]
  def recoverWith(f: PartialFunction[Throwable, Future[T]]): Future[T]
  
These functions return robust features in case current features fail.

Finally, a Future extends from a trait called Awaitable that has two blocking methods, 
ready and result which take the value 'out of' the Future. The signatures of these methods are

  trait Awaitable[T] extends AnyRef {
      abstract def ready(t: Duration): Unit
      abstract def result(t: Duration): T
  }
  
Both these methods block the current execution for a duration of t. 
If the future completes its execution, they return: result returns the actual value 
of the computation, while ready returns a Unit. If the future fails to complete 
within time t, the methods throw a TimeoutException.
*/
  // Await can be used to wait for a future with a specified timeout, e.g.

  println(Await.result(answerToLife, 10 seconds))

  /*
async and await (3rd party library)

Async and await allow to run some part of the code aynchronously. 
The following code computes asynchronously any future inside the await block

  import scala.async.Async._
  
  def retry(noTimes: Int)(block: => Future[T]): Future[T] = async {
    var i = 0;
    var result: Try[T] = Failure(new Exception("Problem!"))
    while (i < noTimes && result.isFailure) {
      result = await { Try(block) }
      i += 1
    }
    result.get
  }

Promises:

A Promise is a monad which can complete a future, with a value if successful 
(thus completing the promise) or with an exception on failure (failing the promise).

trait Promise[T] {
  def future: Future[T]
  def complete(result: Try[T]): Unit  // to call when the promise is completed
  def tryComplete(result: Try[T]): Boolean
}

It is used as follows:

val p = Promise[T]          // defines a promise
p.future                    // returns a future that will complete when p.complete() is called
p.complete(Try[T])          // completes the future
p success T                 // successfully completes the promise
p failure (new <Exception>) // failed with an exception

*/

}