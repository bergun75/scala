package cheatsheet.literals

object MultilineStringExample {
  def main(args: Array[String]):Unit = {
    val multilineExample = """Al sana işte
      multiline"""
    println(multilineExample)
    
    val stripMarginMultiLine = """|Stripped with margin
                                  |you see!""".stripMargin
    println(stripMarginMultiLine)
  }
}