package cheatsheet.literals

import common._

object SymbolLiteralExample {
  def main(args: Array[String]): Unit = {
    require(args.length == 1)
    val count = args(0).toInt
    val sym = 'hi_bro_whats_up_matehi_bro_whats_up_matehi_bro_whats_up_matehi_bro_whats_up_mate
    val sym2 = 'hi_bro_whats_up_matehi_bro_whats_up_matehi_bro_whats_up_matehi_bro_whats_up_mate

    val str = "hi_bro_whats_up_matehi_bro_whats_up_matehi_bro_whats_up_matehi_bro_whats_up_mate"
    val str2 = "hi_bro_whats_up_matehi_bro_whats_up_matehi_bro_whats_up_matehi_bro_whats_up_mate"

    timed {
      println("Symbol")
      for (i <- 0 until count) assert(sym == sym2)
    }

    timed {
      println("String")
      for (i <- 0 until count) assert(str.equalsIgnoreCase(str2))
    }
    /*
     * I FOUND THE BELOW EXPRESSION But couldnt verify it YET!
      *	Symbols are used where you have a closed set of identifiers that you want to be able to compare quickly. 
      * When you have two String instances they are not guaranteed to be interned[1], so to compare them you must 
      * often check their contents by comparing lengths and even checking character-by-character whether they are the same. 
      * With Symbol instances, comparisons are a simple eq check (i.e. == in Java), 
      * so they are constant time (i.e. O(1)) to look up.
     */
  }
}