package cheatsheet.literals

object XmlModeExample {

  def main(args: Array[String]): Unit = {
    val xmlExample = <book id='01'>
                       <name>
                         Banane
                       </name>
                     </book>
    println(xmlExample.label)
    println(xmlExample.attributes)
    xmlExample.child.foreach { ch => println(ch.label)}

  }
}