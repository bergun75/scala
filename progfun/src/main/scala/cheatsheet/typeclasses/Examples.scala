package cheatsheet.typeclasses

import scala.annotation.implicitNotFound

import cheatsheet.forcomprehensions.randomgenerator.Generator

object Examples extends App {
  val integers = new Generator[Int] {
    val prg = java.util.concurrent.ThreadLocalRandom.current()
    def generate: Int = prg.nextInt()
  }
  
  val randomNumber = (1 to 1000) map (_ => integers.generate)
  implicit val intlike = Math.NumberLike.NumberLikeInt
  println(Statistics.mean(randomNumber.toVector))
}

object Math {
  import annotation.implicitNotFound
  @implicitNotFound("No member of type class NumberLike in scope for ${T}")
  trait NumberLike[T] {
    def plus(x: T, y: T): T
    def divide(x: T, y: Int): T
    def minus(x: T, y: T): T
  }
  object NumberLike {
    implicit object NumberLikeDouble extends NumberLike[Double] {
      def plus(x: Double, y: Double): Double = x + y
      def divide(x: Double, y: Int): Double = x / y
      def minus(x: Double, y: Double): Double = x - y
    }
    implicit object NumberLikeInt extends NumberLike[Int] {
      def plus(x: Int, y: Int): Int = x + y
      def divide(x: Int, y: Int): Int = x / y
      def minus(x: Int, y: Int): Int = x - y
    }
  }
}

object Statistics {
  import Math.NumberLike
  def mean[T](xs: Vector[T])(implicit ev: NumberLike[T]): T =
    ev.divide(xs.reduce(ev.plus(_, _)), xs.size)
}
