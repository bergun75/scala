package cheatsheet.classes.hierarchies

object Examples extends App {
  val level1 = new Level1  
  println(level1.method1(2))
  println(MyObject.method1(2))
}

abstract class TopLevel { // abstract class  
  def method1(x: Int): Int // abstract method  
  def method2(x: Int): Int = { x - 1 }
}

class Level1 extends TopLevel {
  def method1(x: Int): Int = { x }
  override def method2(x: Int): Int = { x + 3 } // TopLevel's method2 needs to be explicitly overridden  
}

object MyObject extends TopLevel { // defines a singleton object. No other instance can be created
  def method1(x: Int): Int = { x + 2 }
} 