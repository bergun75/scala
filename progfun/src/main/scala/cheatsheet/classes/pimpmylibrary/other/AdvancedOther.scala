package cheatsheet.classes.pimpmylibrary.other


trait StupidSupport {
  implicit def somethingStupid(other: Other): AdvancedOther = new AdvancedOther(other)
  def somethingStupid(other: Other, str: String): Unit = other.somethingStupid(str)
}

final class AdvancedOther(value: Other) {
  def somethingStupid(str: String): Unit = { println(s"very stupid $str") }
}
