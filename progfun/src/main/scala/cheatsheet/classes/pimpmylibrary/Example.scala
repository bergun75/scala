package cheatsheet.classes.pimpmylibrary

import scala.reflect.ClassTag
/*
 * A ClassTag[T] stores the erased class of a given type T, accessible via the runtimeClass field. 
 * This is particularly useful for instantiating Arrays whose element types are unknown at compile time.
 *
 * ClassTags are a weaker special case of scala.reflect.api.TypeTags#TypeTags, in that they wrap 
 * only the runtime class of a given type, whereas a TypeTag contains all static type information. 
 * That is, ClassTags are constructed from knowing only the top-level class of a type, without necessarily 
 * knowing all of its argument types. This runtime information is enough for runtime Array creation.
 */

object Example extends App {
  implicit def enrichArray[T: ClassTag](xs: Array[T]) = new RichArray[T](xs)
  implicit def enrichList[T: ClassTag](xs: List[T]) = new RichList[T](xs)

  val x = Array(1, 2, 3)
  val y = Array(4, 5, 6)
  val z = x append y
  z.foreach { x => println(x) }

  val xL = List(11, 12, 13)
  val yL = List(45, 46, 47)
  val zL = xL fancyOp yL
  zL.foreach { x => println(x) }
}

class RichArray[T: ClassTag](value: Array[T]) {
  def append(other: Array[T]): Array[T] = {
    val result = new Array[T](value.length + other.length)
    Array.copy(value, 0, result, 0, value.length)
    Array.copy(other, 0, result, value.length, other.length)
    result
  }
}

class RichList[T: ClassTag](value: List[T]) {
  def fancyOp(other: List[T]): List[T] = {
    value ::: other
  }
}