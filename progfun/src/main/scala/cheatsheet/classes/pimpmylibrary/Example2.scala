package cheatsheet.classes.pimpmylibrary

import cheatsheet.classes.pimpmylibrary.other.Other
import cheatsheet.classes.pimpmylibrary.other.somethingStupid



/*
 * Explanation of pimping library from other packages approach:
 * Assume the other package is cheatsheet.implicits.other and the class that will be
 * pimped over is Other and you are trying to add a function called somethingStupid
 * without changing class Other.
 * 
 * Pimping implicit definitions are in trait StupidSupport.
 * And functions are exported using package object other See all the files
 * to understand 
 * 
 */

object Example2 extends App {
  val v = new Other
  v.something
  v.somethingStupid("Bob")

}

