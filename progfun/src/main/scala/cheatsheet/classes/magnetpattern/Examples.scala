package cheatsheet.classes.magnetpattern

// Magnet pattern for overloaded methods

object Examples extends App {
  def magnetMethod(magnet: Magnet): Unit = println(magnet)
  implicit def fromStringAndInt(tuple: (String, Int)) = new Magnet(s"Implementation X: (String, Int) = (${tuple._1}, ${tuple._2})")
  implicit def fromShit(some: Shit) = new Magnet(s"Implementation Shit: (String, Int) = ($some)")
  magnetMethod("boris", 42)
  magnetMethod(Shit("boris"))
  
}

class Magnet(string: String) {
  override def toString() = string
}

case class Shit(str: String)
