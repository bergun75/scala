package cheatsheet.classes.sealeds

object ExhaustivenessCheckExample {
  val x: Option[String] = ???
  val y = x match {
    case Some(d) => d 
  }
  // this exhaustive check is because the above expression when translated to its
  // structural recursion the compiler discovers that not all the cases are handled
  // thus shouts at us! This because Option is marked as sealed to provide this
  // functionality
}

