package cheatsheet.classes.sealeds

object Examples extends App{

}

sealed class A

class B extends A
final class C extends A // Best practice to make it not subclassable or extendable!