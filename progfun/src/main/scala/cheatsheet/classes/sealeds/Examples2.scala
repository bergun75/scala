package cheatsheet.classes.sealeds

object Examples2 {
  
}

 
// class D extends A illegal inheritance sealed class cannot be subclassed from a different file
class D extends B // although an unsealed subclass of a sealed class can be!
// Be carefull about this when sealing your classes.