package cheatsheet.classes

object Examples extends App {
  new MyClass(0, 2) // creates a new object of type

}
// this references the current object, assert(<condition>) issues
// AssertionError if condition is not met. See scala.Predef for require, assume and assert.
class MyClass(x: Int, y: Int) { // Defines a new type MyClass with a PRIMARY constructor  
  require(y > 0, "y must be positive") // precondition, triggering an IllegalArgumentException if not met  
  def this(x: Int) = { this(x, 0) } // AUXILIARY constructor
  // Cannot use super keyword in AUXILIARY constructor
  def nb1 = x // public method computed every time it is called  
  def nb2 = y
  private def test(a: Int): Int = { ??? } // private method  
  val nb3 = x + y // computed only once  
  override def toString = // overridden method  
    x + ", " + y
}

    