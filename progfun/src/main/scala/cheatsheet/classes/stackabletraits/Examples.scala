package cheatsheet.classes.stackabletraits

import scala.collection.mutable.ArrayBuffer

object Examples extends App {
  println("new BasicIntQueue with Filtering with Incrementing")
  val queue = new BasicIntQueue with Filtering with Incrementing
  queue.put(-1)
  queue.put(0)
  queue.put(1)
  println(queue.get())
  println(queue.get())
  println(queue.get())
  println("new BasicIntQueue with Incrementing with Filtering")
  val queue2 = new BasicIntQueue with Incrementing with Filtering
  queue2.put(-1)
  queue2.put(0)
  queue2.put(1)
  println(queue2.get())
  println(queue2.get())
  println(queue2.get())


}

abstract class IntQueue {
  def get(): Option[Int]

  def put(x: Int)
}

class BasicIntQueue extends IntQueue {
  private val buf = new ArrayBuffer[Int]

  override def get(): Option[Int] = if (buf.size > 0) Some(buf.remove(0)) else None

  override def put(x: Int) = buf += x
}

trait Doubling extends IntQueue {
  abstract override def put(x: Int) {
    super.put(2 * x)
  }
}

trait Incrementing extends IntQueue {
  abstract override def put(x: Int) {
    super.put(x + 1)
  }
}

trait Filtering extends IntQueue {
  abstract override def put(x: Int) {
    if (x >= 0) super.put(x)
  }
}