package cheatsheet.classes.organization

/*
 Classes and objects are organized in packages (package myPackage).

They can be referenced through import statements 
(
 import myPackage.MyClass, 
 import myPackage._, 
 import myPackage.{MyClass1, MyClass2}, 
 import myPackage.{MyClass1 => A}
)

They can also be directly referenced in the code with the 
fully qualified name (new myPackage.MyClass1)

All members of packages scala and java.lang as well as all members of the 
object scala.Predef are automatically imported.

Traits are similar to Java interfaces, except they can have non-abstract members:

General object hierarchy:

scala.Any base type of all types. Has methods hashCode 
and toString that can be overridden

scala.AnyVal base type of all primitive types. 
(scala.Double, scala.Float, etc.)

scala.AnyRef base type of all reference types. 
(alias of java.lang.Object, supertype of java.lang.String, scala.List, any user-defined class)

scala.Null is a subtype of any scala.AnyRef 
(null is the only instance of type Null), and scala.Nothing is a subtype of any other type without any instance. 
*/

import cheatsheet.classes.hierarchies.{Level1 => L}
import cheatsheet.classes.hierarchies.{MyObject => O}

object Examples extends App {
  val level1 = new L
  println(level1.method1(2))
  println(O.method1(2))  
}

trait Planar
{
  def method1: Int = 3 // this is not possible in Java!
  def method2: Int
}
