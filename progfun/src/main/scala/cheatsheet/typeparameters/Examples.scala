package cheatsheet.typeparameters

object Examples {
  new MyClass[Int](1)
  new MyClass(1) // the type is being inferred, i.e. determined based on the value arguments
  val level1: Level1 = ???
  val level2: Level2 = ???
  val levelTop: TopLevel = ???
  val myClass = new MyClass(1)
  myClass.myFct(levelTop)
  myClass.myFct2(level1)
  myClass.myFct2(level2)
  myClass.myFct2(levelTop)
  myClass.myFct3(level2)
  myClass.myFct3(levelTop)

}

// Conceptually similar to C++ templates or Java generics. 
// These can apply to classes, traits or functions.

class MyClass[T](arg1: T) {
  // It is possible to restrict the type being used, e.g.
  def myFct[T <: TopLevel](arg: T): T = { ??? } // T must derive from TopLevel or be TopLevel
  def myFct2[T >: Level1](arg: T): T = { ??? } // T must be a supertype of Level1
  def myFct3[T >: TopLevel <: Level2](arg: T): T = { ??? }
}

abstract class Level1

abstract class Level2 extends Level1

class TopLevel extends Level2
     