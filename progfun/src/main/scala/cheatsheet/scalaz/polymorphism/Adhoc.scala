package cheatsheet.scalaz.polymorphism

object Adhoc extends App{
  // A: Plus being the context bound Type parameter
  def plus[A: Plus](a1: A, a2: A): A = implicitly[Plus[A]].plus(a1, a2)
  // same as
  def plus2[A <: Plus[A]](a1: A, a2: A)(implicit m: Plus[A]): A = m.plus(a1, a2)
}

trait Plus[A]{
  def plus(a1: A, a2:A):A
}