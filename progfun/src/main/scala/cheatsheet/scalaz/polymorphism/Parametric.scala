package cheatsheet.scalaz.polymorphism
/*
 * Parametric polymorphism refers to when the type of a value contains one or more (unconstrained) type variables, 
 * so that the value may adopt any type that results from substituting those variables with concrete types. */
object Parametric extends App {
  def head[A](xs: List[A]): Option[A] = xs match {
    case x :: xs => Some(x)
    case Nil     => None
  }
  val list1 = 1 :: 2 :: Nil
  val list2 = "a" :: "b" :: Nil
  val list3 = 1 :: "b" :: Nil
  val x = head(list1)
  val y = head(list2)
  val z = head(list3)
}