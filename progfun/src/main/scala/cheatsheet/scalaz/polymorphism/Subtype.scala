package cheatsheet.scalaz.polymorphism

object Subtype extends App {
  def plus[A <: PlusSub[A]](a1: A, a2: A): A = a1.plus(a2)

}

trait PlusSub[A] {
  def plus(a2: A): A
}
/*
 * We can at least provide different definitions of plus for A. 
 * But, this is not flexible since trait Plus needs to be mixed in at the time of defining the datatype. 
 * So it can’t work for Int and String.
 */
