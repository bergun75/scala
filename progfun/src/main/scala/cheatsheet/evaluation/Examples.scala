

package cheatsheet.evaluation

object Examples extends App {

  def example1 = {
    println("evaluated when called example1")
    1
  }
  val example2 = {
    println("evaluated immediately example2")
    2
  }
  lazy val example3 = {
    println("evaluated once when needed example3")
    3
  }

  def myFct(something: String, bindings: Int*) = {
    println(something)
    println(bindings(0))
    println(bindings(1))
  }

  println(s"example1=$example1")
  println(s"example1=$example1")
  println(s"example2=$example2")
  println(s"example2=$example2")
  println(s"example3=$example3")
  println(s"example3=$example3")
  myFct("boris", Int.MinValue, Int.MaxValue)
}