package cheatsheet.highorderfunctions

//These are functions that take a function as a parameter or return functions.
object Examples extends App {
  // sum() returns a function that takes two integers and returns an integer  
  def sum(f: Int => Int): (Int, Int) => Int = {
    def sumf(a: Int, b: Int): Int = {
      if (a == b) f(a)
      else sumf(a + 1, b) + f(a)
    }
    sumf
  }

  // This is a shorter and better declaration of the above function
  def sumBetter(f: Int => Int)(a: Int, b: Int): Int = {
    if (a == b) f(a)
    else sumBetter(f)(a + 1, b) + f(a)

  }

  val sumResult = sum(x => x * x)(1, 3)
  println(sumResult)
  val sumResultBetter = sumBetter(x => x * x)(1, 3)
  println(sumResultBetter)
}
