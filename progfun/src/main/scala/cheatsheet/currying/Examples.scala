package cheatsheet.currying

// Converting a function with multiple arguments into a function with a 
// single argument that returns another function.
object Examples extends App {
  // uncurried version (type is (Int, Int) => Int)
  def f(a: Int, b: Int): Int =
    {
      if (a == b) a
      else f(a + 1, b) + a
    }

  // curried version (type is Int => Int => Int)
  def f2(a: Int)(b: Int): Int =
    {
      if (a == b) a
      else f2(a + 1)(b) + a
    }

  println(f(1, 10))
  println(f2(1)(10))
}