package cheatsheet.collections

/*
Scala defines several collection classes:

Base Classes
  Iterable (collections you can iterate on)
  Seq (ordered sequences)
  Set
  Map (lookup data structure)
  
Immutable Collections

  List (linked list, provides fast sequential access)
  Stream (same as List, except that the tail is evaluated only on demand)
  Vector (array-like type, implemented as tree of blocks, provides fast random access)
  				it is actually 32 branched trie with little endian bitmap approach
  Range (ordered sequence of integers with equal spacing)
  String (Java type, implicitly converted to a character sequence, so you can treat every string like a Seq[Char])
  Map (collection that maps keys to values)
  Set (collection without duplicate elements)

Mutable Collections

  Array (Scala arrays are native JVM arrays at runtime, therefore they are very performant)
	Scala also has mutable maps and sets; these should only be used if there are performance issues with immutable types
*/

object Examples extends App {
    val fruitList = List("apples", "oranges", "pears")
    // Alternative syntax for lists
    val fruit = "apples" :: ("oranges" :: ("pears" :: Nil)) // parens optional, :: is right-associative
    fruit.head   // "apples"
    fruit.tail   // List("oranges", "pears")
    val empty = List()
    val empty2 = Nil

    val nums = Vector("louis", "frank", "hiromi")
    nums(1)                     // element at index 1, returns "frank", complexity O(log(n))
    nums.updated(2, "helena")   // new vector with a different string at index 2, complexity O(log(n))
    
    val fruitSet = Set("apple", "banana", "pear", "banana")
    fruitSet.size    // returns 3: there are no duplicates, only one banana

    val r: Range = 1 until 5 // 1, 2, 3, 4
    val s: Range = 1 to 5    // 1, 2, 3, 4, 5
    1 to 10 by 3  // 1, 4, 7, 10
    6 to 1 by -2  // 6, 4, 2

    val s1 = (1 to 6).toSet
    s1 map (_ + 2) // adds 2 to each element of the set

    val s2 = "Hello World"
    s2 filter (c => c.isUpper) // returns "HW"; strings can be treated as Seq[Char]

    // Operations on sequences
    val xs = List[Int]()
    val ys = List[Int]()
    val zs = List(List[Int]())
    xs.length   // number of elements, complexity O(n)
    xs.last     // last element (exception if xs is empty), complexity O(n)
    xs.init     // all elements of xs but the last (exception if xs is empty), complexity O(n)
    xs take 6   // first 6 elements of xs
    xs drop 3   // the rest of the collection after taking 3 elements
    xs(4)       // the 4th element of xs, complexity O(n)
    xs ++ ys    // concatenation, complexity O(n)
    xs.reverse  // reverse the order, complexity O(n)
    xs updated(4, 3)  // same list than xs, except at index 4 where it contains 3, complexity O(n)
    xs indexOf 3      // the index of the first element equal to 3 (-1 otherwise)
    xs contains 3     // same as xs indexOf 3 >= 0
    xs filter (x => x > 0)       // returns a list of the elements that satisfy the predicate p
    xs filterNot (x => x > 0)    // filter with negated p 
    xs partition (x => x > 0)    // same as (xs filter p, xs filterNot p)
    xs takeWhile (x => x > 0)    // the longest prefix consisting of elements that satisfy p
    xs dropWhile (x => x > 0)    // the remainder of the list after any leading element satisfying p have been removed
    xs span (x => x > 0)         // same as (xs takeWhile p, xs dropWhile p)
    
    //List(x1, ..., xn) reduceLeft op    // (...(x1 op x2) op x3) op ...) op xn
    //List(x1, ..., xn).foldLeft(z)(op)  // (...( z op x1) op x2) op ...) op xn
    //List(x1, ..., xn) reduceRight op   // x1 op (... (x{n-1} op xn) ...)
    //List(x1, ..., xn).foldRight(z)(op) // x1 op (... (    xn op  z) ...)
    
    xs exists (x => x > 0)    // true if there is at least one element for which predicate p is true
    xs forall (x => x > 0)    // true if p(x) is true for all elements
    xs zip ys      // returns a list of pairs which groups elements with same index together
    // xs unzip      // opposite of zip: returns a pair of two lists    
    val res = xs.flatMap ( x => List(x, 0))   // applies the function to all elements and concatenates the result
    xs.sum         // sum of elements of the numeric collection
    xs.product     // product of elements of the numeric collection
    xs.max         // maximum of collection
    xs.min         // minimum of collection
    //xs.flatten     // flattens a collection of collection into a single-level collection
    xs groupBy (x => x % 2)   // returns a map which points to a list of elements
    //xs distinct    // sequence of distinct entries (removes duplicates)

    3 +: xs  // creates a new collection with leading element 3
    xs :+ 3  // creates a new collection with trailing element 3

    // Operations on maps
    val myMap = Map("I" -> 1, "V" -> 5, "X" -> 10)  // create a map
    myMap("I")      // => 1  
    myMap("A")      // => java.util.NoSuchElementException  
    myMap get "A"   // => None 
    myMap get "I"   // => Some(1)
    myMap.updated("V", 15)  // returns a new map where "V" maps to 15 (entry is updated)
                            // if the key ("V" here) does not exist, a new entry is added

    // Operations on Streams
    val strm = Stream(1, 2, 3)
    val strm1 = Stream.cons(1, Stream.cons(2, Stream.cons(3, Stream.empty))) // same as above
    (1 to 1000).toStream // => Stream(1, ?)
    3 #:: strm // Same as Stream.cons(x, xs)
             // In the Stream's cons operator, the second parameter (the tail)
             // is defined as a "call by name" parameter.
             // Note that x::xs always produces a List
}