package cheatsheet.collections.treemap

import scala.collection.immutable.TreeMap

object Examples extends App{
  val treeMap = TreeMap(2 -> "tendu")
  val newTreeMap = treeMap + (3 -> "baris") + (1 -> "poyraz")
  val seq = newTreeMap.foldLeft(Nil: List[(Int,String)])((acc, x) => (x :: acc))
  val seq2 = newTreeMap.foldRight(Nil: List[(Int,String)])((x, acc) => (x :: acc))
  println(seq)
  println(seq2)
}