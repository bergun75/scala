package week6

object PrimeTest extends App{
  def isPrime(n: Int): Boolean = (2 until n) forall (d => n % d != 0)
  
  println(isPrime(97))
  println(isPrime(32))
}