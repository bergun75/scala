package parprog.week1

import scala.util.Random
import common2.parallel
import common._

object EstimatePi extends App {
  def mcCount(iter: Int): Int = {
    val rX = new Random
    val rY = new Random
    var hits = 0
    for (i <- 0 until iter) {
      val x = rX.nextDouble
      val y = rY.nextDouble
      if (x * x + y * y < 1) hits = hits + 1
    }
    hits
  }
  def monteCarloPiSeq(iter: Int): Double = 4.0 * mcCount(iter) / iter
  def monteCarloPiPar(iter: Int): Double = {
    val ((pi1, pi2), (pi3, pi4)) = parallel(
      parallel(mcCount(iter / 4), mcCount(iter / 4)),
      parallel(mcCount(iter / 4), mcCount(iter / 4)))
    4.0 * (pi1 + pi2 + pi3 + pi4) / iter
  }

  println(timed { monteCarloPiSeq(100000000) })
  println(timed { monteCarloPiPar(100000000) })
}