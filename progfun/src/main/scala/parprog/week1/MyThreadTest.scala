package parprog.week1

class MyThread extends Thread {

  override def run() {
    val uids = for (i <- 0 until 10) yield ThreadSynchronization.getUniqueId()
    println(uids)
  }
}

object ThreadSynchronization extends App {
  private val x = new AnyRef

  private var uidCount = 0L

  def getUniqueId(): Long = x.synchronized {
    uidCount = uidCount + 1
    uidCount
  }

  def startThreads() = {
    val t = new MyThread
    val t2 = new MyThread
    t.start()
    t2.start()
    t.join()
    t2.join()
  }

  startThreads()

}