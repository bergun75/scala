package week4

import scala.annotation.tailrec

object Naturals extends App {
  val zero: Nat = Zero
  val sixty = createNaturalNumber(60)
  println(sixty)
  val seventy = createNaturalNumber(70)
  println(seventy)
  println(seventy + sixty)
  println(seventy - sixty)
  println(sixty - seventy)

  def createNaturalNumber(n: Int): Nat = {
    @tailrec
    def loop(cur: Int, num: Nat): Nat = {
      if (cur == n) num
      else loop(cur + 1, num.successor)
    }
    loop(0, zero)
  }
}

abstract class Nat {
  def isZero: Boolean
  def predecessor: Nat
  def successor: Nat = new Succ(this)
  def +(that: Nat): Nat
  def -(that: Nat): Nat
  override def toString() = {
    @tailrec
    def loop(num: Nat, cur: Int): Int = {
      if (num.isZero) cur
      else loop(num.predecessor, cur + 1)
    }
    loop(this, 0).toString()
  }
}

object Zero extends Nat {
  def isZero: Boolean = true
  def predecessor: Nat = throw new NoSuchElementException("Negatives not supported")
  def +(that: Nat): Nat = that
  def -(that: Nat): Nat =
    if (that.isZero) this
    else throw new NoSuchElementException("Negatives not supported")
}
class Succ(n: Nat) extends Nat {
  def isZero: Boolean = false
  def predecessor: Nat = this.n
  def +(that: Nat): Nat =
    if (that.isZero) this
    else this.successor + that.predecessor
  def -(that: Nat): Nat =
    if (that.isZero) this
    else this.predecessor - that.predecessor
}