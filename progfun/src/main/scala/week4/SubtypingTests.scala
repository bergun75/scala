package week4

object SubtypingTests {

  trait Function1[-T, +U] {
    def apply(x: T): U
  }

}