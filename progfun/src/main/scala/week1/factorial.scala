package week1
import scala.annotation.tailrec

object factorial extends App {
  def factorial(n: Int) = {
    @tailrec
    def factorial(res: Int, n: Int): Int =
      if (n == 0)
        res
      else
        factorial(n * res, n - 1)
    factorial(1, n)
  }
  println(factorial(3))
}