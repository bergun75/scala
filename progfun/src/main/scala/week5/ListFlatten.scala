package week5

object ListFlatten extends App {
  def flatten(xs: List[Any]): List[Any] = {
    def loop(x: Any): List[Any] = {
      x match {
        case List()  => Nil
        case x :: ys => loop(x) ::: loop(ys)
        case y     => List(y) 
      }
    }
  loop(xs)
  }
  println(flatten(List(List(1, 1), 'A', List(3, List(5, 8)))))
}