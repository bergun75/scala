package week5

object SquareList extends App {

  def squareList(xs: List[Int]): List[Int] =
    xs match {
      case Nil     => xs
      case y :: ys => y * y :: squareList(ys)
    }

  def squareList2(xs: List[Int]): List[Int] =
    xs map (x => x * x)
  
  println(squareList(List(1,2,3,4)))
  println(squareList2(List(1,2,3,4)))

}