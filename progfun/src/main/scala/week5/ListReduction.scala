package week5

object ListReduction extends App {

  def mapFun[T, U](xs: List[T], f: T => U): List[U] =
    (xs foldRight List[U]())( (x, y) => f(x) :: y)

  def lengthFun[T](xs: List[T]): Int =
    (xs foldRight 0)((_, y) => y + 1)

  println(lengthFun(List("a", "a", "a", "b", "c", "c", "a")))
  println(mapFun(List(1, 2, 3, 4, 5, 6, 7), (x: Int) => x + 1.2))

}