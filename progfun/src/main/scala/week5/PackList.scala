package week5

object PackList extends App {

  def pack[T](xs: List[T]): List[List[T]] = xs match {
    case Nil => Nil
    case x :: xs1 =>
      val (first, rest) = xs span (y => y == x)
      first :: pack(rest)
  }

  def encode[T](xs: List[T]): List[(T, Int)] = {
    val packList = pack(xs)
    def loop[T](xsl: List[List[T]]): List[(T, Int)] = {
      xsl match {
        case Nil => Nil
        case x :: xsl1 =>
          (x(0), x.length) :: loop(xsl1)

      }
    }
    loop(packList)
  }
  
  def encode2[T](xs: List[T]): List[(T, Int)] = {
    pack(xs) map (ys => (ys.head, ys.length))
    
  }
  println(pack(List("a", "a", "a", "b", "c", "c", "a")))
  println(encode(List("a", "a", "a", "b", "c", "c", "a")))
  println(encode2(List("a", "a", "a", "b", "c", "c", "a")))

}