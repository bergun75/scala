package week5

object ListMerge extends App{
  def merge(xs: List[Int], ys: List[Int]): List[Int] =
  (xs, ys) match {
    case (Nil, ys1) => ys1
    case (xs1, Nil) => xs1
    case (x:: xs2, y:: ys2) => 
      if(x < y) x :: merge(xs2, ys) 
      else y :: merge(xs, ys2)
  }  
  
  println(merge(List(2,4,6,8), List(1,3,5,7,9)))
}