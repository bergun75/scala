package week3

import scala.annotation.tailrec

object nth extends App{
  def nth[T](n: Int, list: List[T]): T = {
    @tailrec
    def loop(index: Int, list: List[T]): T = {
      if (index == n) list.head
      else if (list.tail.isEmpty) throw new IndexOutOfBoundsException(n + "is out of range")
      else loop(index + 1, list.tail)
    }
    loop(0, list)
  }
  val xs:List[Int] =new Cons(1, new Cons(2, new Cons(3, new Nil))) 
  println(nth(2, xs))
  println(nth(3, xs))
  
}