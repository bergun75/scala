package week3
import scala.annotation.tailrec

object rationals extends App {
  val x = new Rational(1, 3)
  println(x.numer)
  println(x.denom)

  val y = new Rational(5, 7)
  println(x)
  println(y)
  println(x + y)
  println(!x)
  println(x - y)

  val z = new Rational(3, 2)
  println(x - y - z)
  println(y + y)
  println(x < y)
  println(x max y)

}

class Rational(x: Int, y: Int) {
  require(y != 0, "denominttor must be non zero")

  def this(x: Int) = this(x, 1) // way of defining another constructor

  private val g = gcd(x, y)
  val numer = x / g
  val denom = y / g

  def +(that: Rational) = new Rational(this.numer * that.denom + that.numer * this.denom,
    denom * that.denom)

  def -(that: Rational) = this + !that

  def unary_! : Rational = new Rational(-numer, denom)

  def <(that: Rational) = numer * that.denom < that.numer * denom

  def max(that: Rational) = if (this < that) that else this

  @tailrec
  private def gcd(a: Int, b: Int): Int =
    if (b == 0)
      a
    else
      gcd(b, a % b)

  override def toString = numer + "/" + denom
}