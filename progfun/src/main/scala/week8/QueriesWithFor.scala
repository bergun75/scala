package week8

case class Book(title: String, authors: List[String])
object QueriesWithFor extends App {
  val books: List[Book] = List(
    Book(title = "Structure and Interpretation of Computer Programs",
      authors = List("Abelson, Harald", "Sussman, Gerald j.")),
    Book(title = "Introduction to Functional Programming",
      authors = List("Bird, Richard", "Wadler, Phil")),
    Book(title = "Effective Java",
      authors = List("Bloch, Joshua")),
    Book(title = "Effective Java 2",
      authors = List("Bloch, Joshua")),
    Book(title = "Java Puzzlers",
      authors = List("Bloch, Joshua", "Gafter, Neal")),
    Book(title = "Programming in Scala",
      authors = List("Odersky, Martin", "Spoon, Lex", "Venners, Bill")))

  val bookSet: Set[Book] = Set(
    Book(title = "Structure and Interpretation of Computer Programs",
      authors = List("Abelson, Harald", "Sussman, Gerald j.")),
    Book(title = "Introduction to Functional Programming",
      authors = List("Bird, Richard", "Wadler, Phil")),
    Book(title = "Effective Java",
      authors = List("Bloch, Joshua")),
    Book(title = "Effective Java 2",
      authors = List("Bloch, Joshua")),
    Book(title = "Java Puzzlers",
      authors = List("Bloch, Joshua", "Gafter, Neal")),
    Book(title = "Programming in Scala",
      authors = List("Odersky, Martin", "Spoon, Lex", "Venners, Bill")))

  val authors = for {
    b1 <- bookSet
    b2 <- bookSet
    if b1.title < b2.title
    a1 <- b1.authors
    a2 <- b2.authors
    if a1 == a2
  } yield a1

  println(authors)

  val bookTitles = for (b <- books; a <- b.authors; if (a indexOf "Bloch") >= 0)
    yield b.title

  println(bookTitles)
  val bookTitlesHO =
    books.flatMap(x => for (a <- x.authors; if (a indexOf "Bloch") >= 0) yield x.title)
  println(bookTitlesHO)

  val bookTitlesHO1 =
    books.flatMap(x => for (a <- x.authors.withFilter(a => (a indexOf "Bloch") >= 0)) yield x.title)
  println(bookTitlesHO1)
  val bookTitlesHO2 =
    books.flatMap(x => x.authors.withFilter(a => (a indexOf "Bloch") >= 0) map (y => x.title))
  println(bookTitlesHO2)

}