package week8

object test extends App {
  val f: String => String = { case "ping" => "pong" }
  val g: PartialFunction[String, String] = { case "ping" => "pong" }
  println(f("ping"))
  println(g.isDefinedAt("ping"))
  println(g.isDefinedAt("asd"))
  println(g("asd"))
  //  f("pong")
}