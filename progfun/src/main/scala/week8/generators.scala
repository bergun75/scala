package week8

trait Generator[+T] {
  self =>

  def generate: T

  def map[S](f: T => S): Generator[S] = new Generator[S] {
    def generate: S = f(self.generate)
  }
  
  def flatMap[S](f: T => Generator[S]): Generator[S] = new Generator[S] {
    def generate: S = f(self.generate).generate
  }
}

trait Tree

case class Inner(left: Tree, right: Tree) extends Tree

case class Leaf(x: Int) extends Tree

object generators extends App {
  val integers = new Generator[Int] {
    def generate = scala.util.Random.nextInt()
  }
  
  println(integers.generate)
  
  val booleans = integers map (_ >= 0)
  
  println(booleans.generate)
  
  def leafs: Generator[Leaf] = for {
    x <- integers
  } yield Leaf(x)
  
  def inners: Generator[Inner] = for {
    l <- trees
    r <- trees
  } yield Inner(l,r)
  
  def trees: Generator[Tree] = for {
    isLeaf <- booleans
    tree <- if (isLeaf) leafs else inners
  } yield tree
  
  println(trees.generate)
  
  def choose(lo: Int, hi: Int): Generator[Int] =
    for(x <- integers) yield lo + (math abs x % (hi - lo))
    
  
  def oneOf[T](xs: T*): Generator[T] =    
    for(idx <- choose(0, xs.length)) yield xs(idx)
  
  println(oneOf(3,4,5,6).generate)
}

