package stackoverflow

import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterAll
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class StackOverflowSuite extends FunSuite with BeforeAndAfterAll {

  trait TestData {
    import StackOverflow._
    val posting0 = Posting(1, 0, scala.None, scala.None, 0, scala.Some("Java"))
    val posting1 = Posting(2, 1, scala.Some(2), scala.Some(0), 1, scala.None)
    val posting2 = Posting(2, 2, scala.Some(2), scala.Some(0), 15, scala.None)
    val posting3 = Posting(1, 3, scala.None, scala.None, 0, scala.Some("Scala"))
    val posting4 = Posting(2, 4, scala.Some(4), scala.Some(3), 20, scala.None)
    val posting5 = Posting(2, 5, scala.Some(4), scala.Some(3), 0, scala.None)
    val posting6 = Posting(1, 6, scala.None, scala.None, 0, scala.Some("NoneExistingLang"))

    val rdd = sc.parallelize(Seq(
      posting0,
      posting1,
      posting2,
      posting3,
      posting4,
      posting5))

    val grouped = Seq(posting0.id -> Iterable((posting0, posting1),
      (posting0, posting2)),
      posting3.id -> Iterable((posting3, posting4),
        (posting3, posting5)))

    val groupedRdd = sc.parallelize(grouped)
    
    val scored = Seq(posting0 -> posting2.score, 
                     posting3 -> posting4.score,
                     posting6 -> 29)
    
    val scoredRdd = sc.parallelize(scored)
  }

  lazy val testObject = new StackOverflow {
    override val langs =
      List(
        "JavaScript", "Java", "PHP", "Python", "C#", "C++", "Ruby", "CSS",
        "Objective-C", "Perl", "Scala", "Haskell", "MATLAB", "Clojure", "Groovy")
    override def langSpread = 50000
    override def kmeansKernels = 45
    override def kmeansEta: Double = 20.0D
    override def kmeansMaxIterations = 120
  }

  def initializeStackOverflow(): Boolean =
    try {
      StackOverflow
      true
    } catch {
      case ex: Throwable =>
        println(ex.getMessage)
        ex.printStackTrace()
        false
    }

  override def afterAll(): Unit = {
    assert(initializeStackOverflow(), " -- did you fill in all the values in WikipediaRanking (conf, sc, wikiRdd)?")
    import StackOverflow._
    sc.stop()
  }

  test("testObject can be instantiated") {
    val instantiatable = try {
      testObject
      true
    } catch {
      case _: Throwable => false
    }
    assert(instantiatable, "Can't instantiate a StackOverflow object")
  }

  test("'groupedPostings' should work for (specific) RDD ") {
    assert(initializeStackOverflow(), " -- did you fill in all the values in StackOverflow (conf, sc, )?")

    new TestData {
      import StackOverflow._
      val res = groupedPostings(rdd).collect()
      assert(res(0)._1 == 0, "res(0)._1 == 0")
      assert(res(0)._2.toArray.apply(0) == (posting0, posting1), "res(0)._2.toArray.apply(0) == (posting0,posting1)")
      assert(res(0)._2.toArray.apply(1) == (posting0, posting2), "res(0)._2.toArray.apply(1) == (posting0,posting2)")

      assert(res(1)._1 == 3, "res(1)._1 == 3")
      assert(res(1)._2.toArray.apply(0) == (posting3, posting4), "res(1)._2.toArray.apply(0) == (posting3,posting4)")
      assert(res(1)._2.toArray.apply(1) == (posting3, posting5), "res(1)._2.toArray.apply(1) == (posting3,posting5)")
    }
  }

  test("'scoredPostings' should work for (specific) RDD ") {
    assert(initializeStackOverflow(), " -- did you fill in all the values in StackOverflow (conf, sc, )?")

    new TestData {
      import StackOverflow._
      val res = scoredPostings(groupedRdd).collect()
      assert(res.size == 2, "res.size == 2")
      assert(res(0)._1.id == 0, "res(0)._1 == 0")
      assert(res(0)._2 == posting2.score, "res(0)._2 == posting2.score")
      assert(res(1)._1.id == 3, "res(1)._1 == 3")
      assert(res(1)._2 == posting4.score, "res(1)._2 == posting4.score")

    }
  }
  
  test("'vectorPostings' should work for (specific) RDD ") {
    assert(initializeStackOverflow(), " -- did you fill in all the values in StackOverflow (conf, sc, )?")

    new TestData {
      import StackOverflow._
      val res = vectorPostings(scoredRdd).collect()
      assert(res.size == 3, "res.size == 3")
      assert(res(0)._1 == 1 /*Java*/ * langSpread, "res(0)._1 == 1 * langSpread")
      assert(res(0)._2 == posting2.score, "res(0)._2 == posting2.score")
      assert(res(1)._1 == 10 /*Scala*/ * langSpread, "res(1)._1 == 10 * langSpread")
      assert(res(1)._2 == posting4.score, "res(1)._2 == posting4.score")
      assert(res(2)._1 == langs.size /*None Existing*/ * langSpread, "res(2)._1 == langs.size * langSpread")
      assert(res(2)._2 == 29, "res(2)._2 == 29")

    }
  }

}
