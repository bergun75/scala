import org.scalacheck.Gen
import org.scalatest.Matchers
import org.scalatest.WordSpec
import org.scalatest.prop.GeneratorDrivenPropertyChecks
import java.net.URLEncoder
import java.net.URLDecoder
import java.nio.file.Paths
import java.nio.file.Path
import java.nio.file.Files

class SamplePropertySpec extends WordSpec
    with Matchers
    with GeneratorDrivenPropertyChecks {
  implicit override val generatorDrivenConfig = PropertyCheckConfiguration(
    minSuccessful = 10000,
    minSize = 0,
    workers = 5)
  val ws = Seq(' ', '\t')
  val unicodeChar: Gen[Char] = Gen.oneOf((256.toChar to 50000.toChar).filter(Character.isDefined(_)))

  val myPathsGen: Gen[String] = Gen.chooseNum(5, 10).flatMap { n =>
    Gen.buildableOfN[String, Char](n, Gen.oneOf(Gen.oneOf(ws), unicodeChar))
  }

  "a path with special chars" should {
    "be url encoded and decoded" when {
      "a file with special char is present" in {
        val dirStr = "target/tmp/"
        val dir = Paths.get(dirStr)
        if (!Files.exists(dir)) {
          Files.createDirectory(dir);
        }
        forAll(myPathsGen) {
          (str: String) =>
            {
              val enc = "UTF-8"
              val strEncoded = URLEncoder.encode(str, enc).replaceAll("\\+", "%20")
              val strDecoded = URLDecoder.decode(strEncoded, enc)
              str should equal(strDecoded)
              val tmpFile = s"${dirStr}${strEncoded}"
              val pw = new java.io.PrintWriter(new java.io.File(tmpFile))
              pw.write(strEncoded)
              pw.close
              val strFromFile = scala.io.Source.fromFile(tmpFile, "UTF-8").getLines.mkString("\n")
              strFromFile should equal(strEncoded)

            }

        }

      }
    }
  }
}